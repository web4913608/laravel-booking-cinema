@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/showtime" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Publish screening time</h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Screen time</label>
                      <input type="text" id="form7Example1" class="form-control" name="screentime" placeholder="13:20"/>

                    </div>

                  </div>




                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Hall</label>
                      {{-- <input type="text" id="form7Example1" class="form-control" name="hall_id" placeholder="Kid hall"/> --}}
                              <select class="custom-select" id="inputGroupSelect02" name='hall_id'>
                            <option selected>Choose a Halls</option>
                            @foreach ($halls as $hall)
                            <option value={{ $hall->id    }} >{{ $hall->Hall_name  }}</option>

                            @endforeach


                          </select>

                    </div>
                  </div>
                  <div class="col">
                    <label class="form-label" for="form7Example1" >Movie</label>
                    {{-- <input type="text" id="form7Example1" class="form-control" name="movie_id" placeholder="Barbie"/> --}}
                    <select class="custom-select" id="inputGroupSelect02" name='movie_id'>
                        <option selected>Choose a movie</option>
                        @foreach ($movies as $movie)
                        <option value={{ $movie->id    }} >{{ $movie->title }}</option>

                        @endforeach


                      </select>


                    {{-- <select class="custom-select" id="inputGroupSelect02" name='movie_id'>
                            <option selected>Choose Your Halls</option>
                            @foreach ($halls as $hall)
                            <option value={{ $hall->id    }} >{{ $hall->Hall_name  }}</option>

                            @endforeach


                          </select> --}}

                  </div>
                  <div class="form-outline mb-4 my-4 w-25">
                    <label class="form-label" for="form7Example7">Movie Date</label>
                    {{-- <input type="text" id="form7Example3" class="form-control" name="movie_date" placeholder="August 29, 2020" /> --}}
                    {{-- <label for="start">Start date:</label> --}}
                    <div>
                        <input type="date" id="start" name="movie_date"
                        value="" class="date form-control"
                        min="2018-01-01" max="2023-12-31">
                    </div>


                  </div>



                </div>
         <input type="submit" value="Publish" class="border-0 p-2 bg-danger text-white "

         style="border-radius: 5px;padding:0px 15px;"
         >




              </form>
            </div>
          </div>
        </div>



      </div>
</form>

  @endsection
