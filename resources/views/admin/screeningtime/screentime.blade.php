@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Screen time management</h1>
        <br>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/showtime/create" class="text-white">Create a screening time</a></li>
            <li class="breadcrumb-item"><a href="/admin/dashboard" class="text-decoration-none"

                style="color:gray"
                >Dashboard</a></li>
        </ol>
        <div class="col-lg-12 col-md-6 p-0 d-lg-flex d-md-block mb-3">


            <form class="form w-100 d-flex" style="max-width: 600px" action="/showtime/search">
                <input class="form-control mr-sm-2 " type="search" placeholder="Search movie title" aria-label="Search"
                 name="title" >
                 <button class="btn btn-danger" type="submit">Search</button>

            </form>



        </div>
        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
            Seat
            </div>
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Sreening time</th>
                    <th scope="col">Hall</th>
                    <th scope="col">Movie</th>
                    <th scope="col">Location</th>
                    <th scope="col">Status</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Trash</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $showtimes as $showtime)


                  <tr>
                    <th scope="row">{{ $showtime->id}}</th>
                    <td>{{ $showtime->screentime}}</td>
                    <td>{{ $showtime->hall->Hall_name}}</td>
                    <td>{{ $showtime->movie->title }}</td>
                    <td>{{ $showtime->hall->location->district}}</td>
                    @if ($showtime->status == 0)
                    <td >on going</td>
                    @else
                    <td >completed</td>
                    @endif

                    <td><a href="/showtime/{{ $showtime->id}}/edit" class="text-white">Edit</a></td>

                    <form action="/showtime/{{ $showtime->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                        Trash

                        </button>


                    </td>
                 </form>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
@endsection
