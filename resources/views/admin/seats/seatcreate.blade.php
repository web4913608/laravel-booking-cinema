@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/seats" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Create Seat </h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    {{-- <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Row</label>
                      <input type="text" id="form7Example1" class="form-control" name="row" placeholder="A-C"/>

                    </div> --}}
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Row per hall ( A - Z)</label>
                      <input type="text" id="form7Example1" class="form-control" name="numrow" placeholder="3"/>

                    </div>
                  </div>



                  <div class="col">
                    {{-- <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Seat Number</label>
                      <input type="text" id="form7Example1" class="form-control" name="number" placeholder="12"/>

                    </div> --}}
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Seats per row ( 1-10 )</label>
                      <input type="text" id="form7Example1" class="form-control" name="numseat" placeholder="1"/>

                    </div>
                  </div>
                  <div class="col">
                    <label class="form-label" for="form7Example1" >Seats Price</label>

                    <input type="text" id="form7Example1" class="form-control" name="price" placeholder="$5.99"/>
                  </div>
                  <div class="col">
                    <label class="form-label" for="form7Example1" >Hall name</label>
                        <select class="custom-select" id="inputGroupSelect02" name='s_id'>
                            <option selected>Choose Your Screen</option>
                            {{-- @foreach ($halls as $hall)
                            <option value={{ $hall->id    }} >{{ $hall->Hall_name  }}</option>

                            @endforeach --}}
                            @foreach ($screentime as $st)
                            <option value={{ $st->id    }} >
                                {{ $st->hall->Hall_name}}
                                {{ $st->screentime }}

                            </option>

                            @endforeach

                          </select>

                  </div>



                </div>
         <input type="submit" value="Create" class="border-0 p-2 bg-danger text-white "

         style="border-radius: 7px"
         >




              </form>
            </div>
          </div>
        </div>



      </div>
</form>

  @endsection
