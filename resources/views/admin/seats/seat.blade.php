@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Seat</h1>
        <br>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/seats/create" class="text-white">Create a seat</a></li>
            <li class="breadcrumb-item"><a href="/admin/dashboard" class="text-decoration-none"

                style="color:gray"
                >Dashboard</a></li>
        </ol>

        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
            Seat
            </div>
               <form action="/seats/location" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg">
                        <div class="card-body d-flex">
                            <select

                            class="form-select form-select" aria-label="Default select example"
                            name="location"
                            onchange="this.form.submit()"
                            >
                                <option selected>Choose location/Campus</option>
                                @foreach ($locations as $location)
                                <option value={{ $location->id }}>{{ $location->district }}</option>

                                @endforeach

                              </select>
                              {{-- <button class="btn btn-danger btn-sm mx-2" type="submit">Search</button> --}}
                        </div>
                    </div>
                </form>
                    {{-- {{ $showtimes }} --}}

                    <div class="col-lg">
                        <form action="/seats/showtime" method="POST">
                            @csrf
                        @isset ($showtimes)
                        <div class="card-body d-flex">
                            <select class="form-select form-select" aria-label="Default select example"

                            name="show_id"
                            onchange="this.form.submit()"

                            >
                                <option selected>Choose screen time</option>
                                @foreach ($showtimes as $showtime)
                                <option value={{ $showtime->id }}>
                                    {{ $showtime->hall->Hall_name }} -
                                    {{ $showtime->screentime }}


                                </option>

                                @endforeach

                              </select>
                              {{-- <button class="btn btn-danger btn-sm mx-2" type="submit">Search</button> --}}
                        </div>
                        @endisset
                    </form>
                    </div>

                </div>




            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Seat Row</th>
                    <th scope="col">Seat Number</th>
                    <th scope="col">Movie date</th>
                    <th scope="col">Screen time</th>
                    <th scope="col">Hall name</th>
                    <th scope="col">Location</th>
                    <th scope="col">Active</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $seats as $seat)


                  <tr>
                    <th scope="row">{{ $seat->id}}</th>
                    <td>{{ $seat->row}}</td>
                    <td>{{ $seat->number}}</td>
                    <td>{{ $seat->movie_date}}</td>

                    <td scope="col">{{ $seat->screentime}}</td>
                    <td scope="col">{{ $seat->Hall_name}}</td>
                    <td scope="col">{{ $seat->district}}</td>
                    @if ($seat->active== 1)
                    <td>true</td>

                    @else
                    <td>Booked</td>
                    @endif

                    <td><a href="/seats/{{ $seat->id}}/edit" class="text-white">Edit</a></td>

                    <form action="/seats/{{ $seat->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                            Delete

                        </button>


                    </td>
                 </form>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
<script>
    console.log("hello world")
</script>
@endsection
