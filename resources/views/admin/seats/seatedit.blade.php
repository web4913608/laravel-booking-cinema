@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/seats/{{  $seats->id }}" method="POST" >
    @csrf
@method('PUT')
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Create Seat </h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Row</label>
                      <input type="text" id="form7Example1" class="form-control" name="row" placeholder="A-C"
                      value ={{  $seats->row }}

                      />

                    </div>

                  </div>




                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Number</label>
                      <input
                      value ={{  $seats->number}}
                       type="text" id="form7Example1" class="form-control" name="number" placeholder="12"/>

                    </div>
                  </div>
                  <div class="col">
                    <label class="form-label" for="form7Example1" >Hall name</label>
                        <select class="custom-select" id="inputGroupSelect02" name='hall_id'>
                            <option selected value= {{  $seats->hall->id    }}>{{ $seats->hall->Hall_name }}</option>
                            @foreach ($halls as $hall)
                            <option value={{ $hall->id    }} >{{ $hall->Hall_name  }}</option>

                            @endforeach


                          </select>

                  </div>



                </div>
         <input type="submit" value="Update" class="border-0 p-2 bg-danger text-white ">




              </form>
            </div>
          </div>
        </div>



      </div>
</form>

  @endsection
