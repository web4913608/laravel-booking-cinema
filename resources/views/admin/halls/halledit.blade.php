@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/halls/{{ $hall->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Publish New Locations</h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Hall name</label>
                      <input type="text" id="form7Example1" class="form-control" name="Hall_name" placeholder="Kid Hall"

                      value={{  $hall->Hall_name }}
                      />

                    </div>
                  </div>
                  <div class="col">
                    <label class="form-label" for="form7Example1" >Location place</label>
                        <select class="custom-select" id="inputGroupSelect02" name="location_id">



                            @foreach ($locations as $location)
                            <option value={{$location->id}}>
                                {{  $location->district}}</option>
                            @endforeach




                          </select>

                  </div>






                </div>
         <!-- Message input -->

                <input type="submit" value="Publish location" class="border-0 p-2 bg-danger text-white ">




              </form>
            </div>
          </div>
        </div>



      </div>
</form>

  @endsection
