@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Hall</h1>
        <br>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/halls/create" class="text-white">Create Hall</a></li>
            <li class="breadcrumb-item active">View All </li>
        </ol>

        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
               Movie
            </div>
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Hall name</th>
                    <th scope="col">Location</th>
                    <th scope="col">City</th>
                    <th scope="col">Address</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $halls as $hall)


                  <tr>
                    <th scope="row">{{ $hall->id}}</th>
                    <td>{{ $hall->Hall_name}}</td>
                    <td>{{ $hall->location->district}}</td>
                    <td>{{ $hall->location->city}}</td>
                    <td>{{ $hall->location->address }}</td>
                    <td><a href="/halls/{{ $hall->id}}/edit" class="text-white">Edit</a></td>

                    <form action="/halls/{{ $hall->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                            Delete

                        </button>


                    </td>
                 </form>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
@endsection
