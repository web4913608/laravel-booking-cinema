@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Movie List</h1>
        <br>
        <div class="col-lg-12 col-md-6 p-0 d-lg-flex d-md-block mb-3">


            <form class="form w-100 d-flex" style="max-width: 600px" action="/movies/search" method="GET">
                <input class="form-control mr-sm-2 " type="search" placeholder="Search movie title" aria-label="Search"
                 name="title" >
                 <button class="btn btn-danger" type="submit">Search</button>

            </form>



        </div>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/movies/create" class="text-white">Post Movie</a></li>
            <li class="breadcrumb-item"><a href="/admin/dashboard" class="text-decoration-none"

                style="color:gray"
                >Dashboard</a></li>
        </ol>

        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
               Movie
            </div>
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Director</th>
                    <th scope="col-lg-3">Description</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Date</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody >
                    @foreach ( $Movies as $movie)


                  <tr id="tr" >
                    <th scope="row" >{{ $movie->id}}</th>
                    <td>{{ $movie->title}}</td>
                    <td >{{ $movie->director}}</td>
                    <td style="max-width:600px">{{ $movie->description}}</td>
                    <td>{{ $movie->duration }}</td>

                    <td style="font-size:14px">{{ $movie->movie_date   }}</td>
                    <td>{{ $movie->genre }}</td>
                    <td><a href="/movies/{{$movie->id}}/edit" class="text-white">Edit</a></td>
                    <td>
                        <form action="/movies/{{$movie->id}}" method="POST">
                            @csrf
                            {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">
                           Delete
                    </button>

                     </form>

                    </td>


                      <!-- Modal -->
           </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
@endsection
