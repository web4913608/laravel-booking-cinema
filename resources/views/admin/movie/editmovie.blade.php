@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/movies/{{ $movie->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
    <div class="row justify-content-center my-3">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Publish Movies</h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >Movie Title</label>
                      <input type="text" id="form7Example1" class="form-control" name="title" placeholder="Movie Title"
                      value={{ trim( $movie->title ) }}

                      />

                    </div>
                  </div>
                  <div class="col">
                    <div class="form-outline">
                        <label class="form-label" for="form7Example2">Director name</label>
                      <input type="text" id="form7Example2" class="form-control" name="director" placeholder="Author or director of the movie"
                      value={{  $movie->director  }}

                      />

                    </div>
                  </div>
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form7Example7">Description</label>
                    <textarea class="form-control" id="form7Example7"

                    rows="4" name="description"

                    wrap="virtual"
                    placeholder="Describe movie"
                    style="white-space: pre-line"
                    >
              {{ trim( $movie->description )}}
                </textarea>

                  </div>

                <!-- Text input -->
                <div class="form-outline mb-4 ">
                    <label class="form-label" for="form7Example3">Movie Duration</label>
                    <br>
                    <input type="text" id="duration" hidden class="form-control" name="duration" placeholder="2h 30min"
                    value={{  $movie->duration }}

                    />
                    <div class="timepicker d-flex">
                        <div class="d-flex">

                            <input type="number" id="hour" name="quantity" min="0" max="5" class="form-control"


                            style="height: 30px;width:70px"
                            >
                            <label for=""  class="mx-2">hours</label>
                          </div>
                          <div class="mx-3 d-flex">

                            <input type="number" id="min"
                            name="quantity" min="0" max="59" class="form-control"
                            style="height: 30px;width:70px"
                            value={{ $movie->duration }}


                            >
                            <label for=""  class="mx-2">minutes</label>
                          </div>

                    </div>


                </div>
                {{-- <div class="form-outline mb-4">
                    <label class="form-label" for="form7Example3">Movie Duration</label>


                </div> --}}

                <!-- Text input -->
                <div class="form-outline mb-4 d-flex" >
                    <div class="gen">
                        <label class="form-label" for="form7Example4">Genre</label>
                        <input type="text" id="form7Example4" class="form-control" name="genre" placeholder="Comedy"
                        value={{  $movie->genre }}

                        />
                    </div>

                  <div class="form-outline mb-4" style="margin-left: 50px">

                    <label class="form-label" for="form7Example7">Movie Date</label>
                    {{-- <input type="text" id="form7Example3" class="form-control" name="movie_date" placeholder="August 29, 2020" /> --}}
                    {{-- <label for="start">Start date:</label> --}}
                    <div>
                        <input type="date" id="start" name="movie_date"
                        value={{  $movie->movie_date  }}
                         class="date form-control"
                        min="2018-01-01" max="2023-12-31">
                    </div>

                  </div>
                  <div class="form-outline mb-4" style="margin-left: 50px">

                    <label class="form-label" for="form7Example7">Movie Status</label>
                    {{-- <input type="text" id="form7Example3" class="form-control" name="movie_date" placeholder="August 29, 2020" /> --}}
                    {{-- <label for="start">Start date:</label> --}}
                    <div>
                        <select class="custom-select w-100 h-5" id="inputGroupSelect01" style="height:40px" name="status">

                            @if ( $movie->status!="on-going")
                            <option value={{ $movie->status }} selected>{{ $movie->status }}</option>
                            <option value="on-going" >On Going</option>

                            @else
                            <option value={{ $movie->status }} selected>{{ $movie->status }}</option>
                            <option value="finished" >Completed</option>
                            @endif


                          </select>
                    </div>

                  </div>

                </div>


                    <div class="input-group mb-3">
                        <label class="form-label" for="form7Example7">Languages</label>
                        <select class="custom-select w-100 h-5" id="inputGroupSelect01" style="height:40px" name="languages">


                          <option value="English" selected>English</option>
                          <option value="Khmer">Khmer</option>
                          <option value="Chinese">Chinese</option>
                        </select>
                      </div>






                <!-- Upload image-->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form7Example4">Movie Thumbnail</label>
                    <br>
                    <img class="card-img-top"
                    src="/images/{{ $movie->file_path }}" alt="poster"
                    style="width: 150px;height:150px"
                    >



                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form7Example4">Upload new Movie Image</label>
                  <input type="file" id="form7Example4" class="form-control" name="file_path" />

                </div>

                <!-- Message input -->


                <input type="submit" value="Save and update" class="border-0 p-2 bg-danger text-white">




              </form>
            </div>
          </div>
        </div>


      </div>
</form>

  @endsection
