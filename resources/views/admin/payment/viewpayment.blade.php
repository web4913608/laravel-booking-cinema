
@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<div class="row my-5">
    <div class="col-lg-4">

        <div class="card-body p-5" style="background-color: #222426">
            <a class="text-white" href="/payment">Back</a>
            <div class="top d-flex justify-content-between my-4">

                <h3 class="card-title text-white font-weight-bold text-uppercase" style="font-weight: bold">

                  Order Detail

                </h3>

                <button class="text-white bg-danger btn" style="font-weight: bold" id="btnprint">

                 print

                </button>
            </div>

            <div class="card-inner card-text d-flex ">
                <div class="">
                    Action
                </div>
                <div class="bar mx-1">
                    |
                </div>

                <div class="mx-1">

                </div>
                <div class="bar mx-1">
                    |
                </div>
                <div class="mx-2">
                    PG-13
                </div>
                <div class="bar mx-1">
                    |
                </div>
                <div class="mx-2">

                </div>
            </div>
            <div class="card-bot text-white d-flex justify-content-between align-items-center">


            </div>
            <div class="price d-flex justify-content-between">
                <p class="text-white">Movie Title</p>


                <p class="text-white"> Fun</p>
            </div>
            <div class="price d-flex justify-content-between">
                <p class="text-white">Customer name</p>


                <p class="text-white"> Siv Sovanpanhavorn</p>
            </div>
            <div class="price d-flex justify-content-between">
                <p class="text-white">Customer email</p>


                <p class="text-white"> Nightpp19@gmail.com</p>
            </div>
            <div class="price d-flex justify-content-between">
                <p class="text-white">Order Date</p>


                <p class="text-white"> 07/13/2023 09:39:30</p>
            </div>
            <div class="price d-flex justify-content-between">
                <p class="text-white">Booking Ticket</p>


                <p class="text-white bg-danger p-1">
                    nykNFBXn
                </p>
            </div>
            <div class="option  my-3">


                <div class="payment">

                    <hr class="text-white">
                    <div class="qty d-flex justify-content-between">
                        <p class="text-white">Seats</p>

                        <div class="text-white">
                                                                    B
                                1
                                                            </div>



                    </div>
                    <div class="payment">
                        <div class="qty d-flex justify-content-between">
                            <p class="text-white">screen time</p>

                            <p class="text-white">
                                9:25
                            </p>



                        </div>
                        <div class="qty d-flex justify-content-between">
                            <p class="text-white">Halls</p>

                            <p class="text-white">
                                Normal Hall
                            </p>



                        </div>
                        <div class="qty d-flex justify-content-between">
                            <p class="text-white">Ticket(s)</p>
                            <p class="text-white">x
                                1</p>

                        </div>


                        <div class="price d-flex justify-content-between">
                            <p class="text-white">Price</p>


                            <p class="text-white">$ 3</p>
                        </div>
                        <div class="qty d-flex justify-content-between">
                            <p class="text-white">Discount</p>
                            <p class="text-white">
                                % 0
                            </p>

                        </div>


                        <div class="total d-flex justify-content-between">
                            <p class="text-white">Total amount</p>

                            <p class="text-white font-weight-bold" style="font-size: 25px">

                                $ 0

                            </p>
                        </div>
                    </div>
                    <div class="payment-select">


                    </div>





                </div>

            </div>





        </div>

    </div>
</div>

@endsection
