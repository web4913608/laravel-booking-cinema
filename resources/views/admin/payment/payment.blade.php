@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Payment details</h1>
        <br>
        <ol class="breadcrumb mb-4">
            {{-- <li class="breadcrumb-item"><a href="/showtime/create" class="text-white">Create a screening time</a></li> --}}
            <li class="breadcrumb-item"><a href="/admin/dashboard" class="text-decoration-none"

                style="color:gray"
                >Dashboard</a></li>
        </ol>

        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
            Seat
            </div>
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Order date</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Total amount</th>

                    <th scope="col">Payment Type</th>
                    <th scope="col">Reservation</th>

                  </tr>
                </thead>
                <tbody>
                    @foreach ( $p as $showtime)


                  <tr>
                    <th scope="row">{{ $showtime->id}}</th>
                    <td>{{ $showtime->created_at}}</td>
                    <td>{{ $showtime->discount}}%</td>
                    <td>${{ $showtime->amount }}</td>
                    <td>{{ $showtime->method}}</td>
                    <td>
                        <a href="/payment/{{ $showtime->id}}"
                             class="text-decoration-none text-white"


                             >View Invoice   </a>

                    </td>
                    {{-- <td><a href="/showtime/{{ $showtime->id}}/edit" class="text-white">Edit</a></td> --}}

                    {{-- <form action="/showtime/{{ $showtime->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                            Delete

                        </button>


                    </td>
                 </form> --}}
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
@endsection
