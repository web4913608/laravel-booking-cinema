@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Cinema Location</h1>
        <br>
        <ol class="breadcrumb mb-4">
      
            <li class="breadcrumb-item"><a href="/locations/create" class="text-white">Publish new location</a></li>
            <li class="breadcrumb-item"><a href="/admin/dashboard" class="text-decoration-none"

                style="color:gray"
                >Dashboard</a></li>
        </ol>

        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
               Movie
            </div>
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">City</th>
                    <th scope="col">Address</th>

                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $locations as $location)


                  <tr>
                    <th scope="row">{{ $location->id}}</th>
                    <td>{{ $location->city}}</td>
                    <td>{{ $location->address}}</td>

                    <td><a href="/locations/{{ $location->id}}/edit" class="text-white">Edit</a></td>

                    <form action="/locations/{{ $location->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                            Delete

                        </button>


                    </td>
                 </form>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
@endsection
