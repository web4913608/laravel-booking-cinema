@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-create')
<form action="/locations" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Publish New Locations</h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">
                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >City / Province</label>
                      <input type="text" id="form7Example1" class="form-control" name="city" placeholder="PhnomPenh"/>

                    </div>
                  </div>
                  <div class="col">
                    <div class="form-outline">
                        <label class="form-label" for="form7Example2">Location Address</label>
                      <input type="text" id="form7Example2" class="form-control" name="address" placeholder="street address" />

                    </div>
                  </div>

                  <div class="col">
                    <div class="form-outline bg-dark">
                        <label class="form-label" for="form7Example1" >District Name</label>
                      <input type="text" id="form7Example1" class="form-control" name="district" placeholder="Toul Tompong 2"/>

                    </div>
                  </div>
                  <div class="col">
                    <div class="form-outline">
                        <label class="form-label" for="form7Example2">Google map link</label>
                      <input type="text" id="form7Example2" class="form-control" name="map_url" placeholder="map" />

                    </div>
                  </div>
                </div>


                <!-- Message input -->

                <input type="submit" value="Publish location" class="border-0 p-2 bg-danger text-white ">




              </form>
            </div>
          </div>
        </div>


      </div>
</form>

  @endsection
