@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie-booking')
<form action="/booking/{{ $reserve->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
    <div class="row justify-content-center my-5">
        <div class="col-md-12 col-lg-8">
          <div class="card mb-4">
            <div class="card-header py-3 bg-dark text-white">
              <h5 class="mb-0">Modify Booking</h5>
            </div>
            <div class="card-body bg-dark text-white">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->
                <div class="row mb-4 bg-dark">

                  <div class="col-lg-12">
                    <label class="form-label" for="form7Example1" >Payment</label>
                        <select class="custom-select" id="inputGroupSelect02" name='paid'>

                            @if ($reserve->paid == 0)
                            <option value={{ $reserve->paid }} selected>Reserved</option>
                            <option value=1 >Paid</option>
                            

                            @else
                            <option value={{ $reserve->paid }} selected>Paid</option>
                            <option value=0 >Reserved</option>
                            @endif

                            {{-- <option value =>Paid</option> --}}






                          </select>

                  </div>






                </div>
         <!-- Message input -->

                <input type="submit" value="Update" class="border-0 p-2 bg-danger text-white ">




              </form>
            </div>
          </div>
        </div>



      </div>
</form>

  @endsection
