@extends('admin.admin')
{{-- @extends('admin.master.masteradmin') --}}
@section('movie')
<main>

    <div class="container-fluid px-4 text-white">
        <h1 class="mt-4 text-white">Booking</h1>
        <br>

        <ol class="breadcrumb mb-4">
            <a href="/admin/dashboard" class="text-decoration-none">

                <li class="breadcrumb-item active">Back </li>
            </a>
            {{-- <li class="breadcrumb-item"> /
                <a href="/seats/create" class="text-white">Modify</a>

            </li> --}}


        </ol>
        <form class="form w-100 d-flex mb-5" style="max-width: 600px" action="/search/ticket" method="GET">
            <input class="form-control mr-sm-2 " type="search" placeholder="Search Ticket" aria-label="Search"
             name="searchticket" onchange="this.form.submit()">
             <button class="btn btn-danger" type="submit">Search</button>

        </form>
        <div class="card mb-4 bg-dark">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
            Seat
            </div>
            <form action="/seats/location" method="POST">
                @csrf
                <div class="row">
                    {{-- <div class="col-lg-6">
                        <div class="card-body d-flex">
                            <select

                            class="form-select form-select-sm" aria-label="Default select example"
                            name="location"
                            onchange="this.form.submit()"
                            >
                                <option selected>Choose location</option>
                                @foreach ($locations as $location)
                                <option value={{ $location->id }}>{{ $location->district }}</option>

                                @endforeach

                              </select>
                              {{-- <button class="btn btn-danger btn-sm mx-2" type="submit">Search</button> --}}
                        {{-- </div>
                    </div> --}}
                </form>




                </div>




            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Movie name</th>
                    <th scope="col">Location</th>
                    <th scope="col">Show Hall</th>
                    <th scope="col">booking date</th>
                    <th scope="col">paid</th>
                    <th scope="col">price</th>
                    <th scope="col">qty</th>
                    <th scope="col">Seat Number</th>
                    <th scope="col">screen time</th>
                    <th scope="col">Booking ticket</th>
                    {{-- <th scope="col">Booking ticket</th> --}}
                    {{-- <th scope="col">Active</th> --}}
                    <th scope="col">Edit</th>
                    {{-- <th scope="col">Trash</th> --}}
                  </tr>
                </thead>
                <tbody>
                    @foreach ( $reserve as $book)


                  <tr>
                    <th scope="row">{{ $book->id}}</th>
                    <td>{{ $book->show->movie->title}}</td>
                    <td>{{ $book->show->hall->location->district}}</td>
                    <td>{{ $book->show->hall->Hall_name}}</td>
                    <td>{{ $book->created_at}}</td>
                    @if ($book->paid == 0)
                    <td >Reserved</td>
                    @else
                    <td >Paid</td>
                    @endif

                    <td>${{ $book->price}}.00</td>
                    <td>{{ $book->qty}}</td>
                    <td>
                @foreach ( $bs as $s )
                        @foreach ($book->seats as $item)

                        {{-- {{ $s->id }} --}}
                        @if ($s->id == $item)
                        {{ $s->row      }}
                        {{ $s->number     }}
                        @endif

                     @endforeach

                 @endforeach

                    </td>
                    {{-- @foreach ($bs as $seat )

                    <td>

                        {{ $seat->id}}




                   </td>
                   @endforeach --}}
                    <td>{{  $book->show->screentime}}</td>
                    {{-- <td>{{  $book->show->screentime}}</td> --}}
                    <td>{{ $book->booking_ticket}}</td>


                    <td><a href="/booking/{{ $book->id}}/edit" class="text-white">Edit</a></td>

                    {{-- <form action="/booking/{{ $book->id}}" method="POST">
                        @csrf
                        {{ method_field('delete') }}
                    <td>
                        <button type=submit class="btn btn-danger text-white p-0">
                          Trash

                        </button>


                    </td>
                 </form> --}}
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>

        <div style="height: 100vh"></div>


    </div>

</main>
<script>
    console.log("hello world")
</script>
@endsection
