@extends('page.layouts.master')


@section('movie-detail')
<form action="/order/post" method="POST" id="formsubmit">
    @csrf
<div class="row align-items-center gx-5 my-5">

    <div class="btn-groupcol-12">
        <button class="btn  text-white " id="selected">Select Seat</button>
        <button class="btn  text-white ">Reserve / Buy</button>
        <button class="btn  text-white ">Payment</button>
    </div>


</div>
<hr>
<div class="container">
    {{-- <div class="row">
        <div class="space d-flex " >
            <div class="boxin bg-danger" style="width: 41px;height:31px">

            </div>
           <p class="lead text-white mx-3">Available</p>

       </div>
       <div class="space d-flex " >
        <div class="boxin " style="width: 41px;height:31px;background-color: #3A4750">

        </div>
       <p class="lead text-white mx-3">Unavailable</p>

   </div>
   <div class="space d-flex " >
    <div class="boxin" style="width: 41px;height:31px;background-color: #EEEEEE">

    </div>
   <p class="lead text-white mx-3">Reserved</p>

 </div> --}}

 <section class="my-5">
    <div class="row">
      <div class="col-lg-7 col-md-6">
        <div class="">

            {{-- <select id="movie">
              <option value="250">Interstellar (Rs. 250)</option>
              <option value="200">Kabir Singh (Rs. 200)</option>
              <option value="150">Duniyadari (Rs. 150)</option>
              <option value="100">Natsamrat (Rs. 100)</option>
            </select> --}}

            <ul class="showcase">
              <li>
                <div class="seats"></div>
                <span class="text-white">Available</span>
              </li>
              <li>
                <div class="seats selected"></div>
                <span class="text-white">Selected</span>
              </li>
              <li>
                <div class="seats occupied"></div>
                <span class="text-white">Reserved</span>
              </li>
            </ul>

            <div class="container-box">
              <div class="screen" style="height:164px">
                <div class="text-center " style="color: black">
                  Screen
                </div>
              </div>
              {{-- @foreach ($seats as $seat)
              <div class="text-white">
                  {{ $seat->row }}

              </div>
              <br>
              @endforeach --}}

              <div class="row justify-center" id="main">


                @foreach ($seats as $seat)


                    @if ($seat->number ==1)
                    <p class="text-white mx-3" >    {{ $seat->row }}</p>

                    @if ($seat->number ==1)
                    <div class="mx-4">
                        {{-- <input type="text" name="" id="" class="movierowh" hidden
                        value={{
                             $seat->row
                            }}> --}}
                    </div>

                    @endif


                    @endif

                    <input type="text" name="show_id" id="" hidden

                    value={{ $seat->s_id}}>

                    @if(    $seat->active ==1)
                    <label class="seat text-white text-center" >
                        <input type="text" name="movie_id" id="" hidden

                        value={{ $movie->id }}>

                        <input type="text" name="" id="" class="movierow" hidden
                        value={{
                             $seat->row
                            }}>


                        <input type="checkbox" name="seat_chart[]"
                        class="chbox bg-white"

                        id="checkb"
                        style="opacity: 0;width:60px;height:60px"

                        value={{ $seat->id }}
                        >

                       {{ $seat->number }}
                    </label>
                    @else
                    <label class="seat text-white text-center bg-white" >
                        <input type="text"  id="" hidden

                    >

                        <input type="text" name="" id=""  hidden
                    >


                        <input type="checkbox"
                        class="chbox bg-white"


                        style="opacity: 0;width:60px;height:60px"


                        >
                        <p class="text-white    ">    {{ $seat->number }}</p>

                    </label>
                    @endif


                @endforeach


            </div>

              <p class=" lead display-6 my-5" style="color:grey  "
              >
              <span class="tl lead "></span>
              {{-- <span id="r" class="text-white"></span> --}}
              <span id="count" class="text-white"> </span>
            </p>
            </div>
          </div>





        </div>


        <div class="col-lg-5 col-md-6 col-sm-9 ">

        <div class="card text-left b-0">

          {{-- <form action="" method="GET" id="getform" > --}}

          <div class="card-body">
            <div class="card-img">

            </div>

            <h2 class="card-title font-weight-500 text-uppercase mb-3">

                {{ $movie->title}}

            </h2>


            <div class="card-inner card-text d-flex ">
                    <div class="">
                        {{ $movie->genre}}
                    </div>
                    <div class="bar mx-1">

                    </div>

                    <div class="mx-1">
                        {{ $movie->duration}}
                    </div>
                    <div class="bar mx-1">
                        |
                    </div>
                    <div class="mx-2">
                     PG-13
                    </div>
                    <div class="bar mx-1">
                        |
                    </div>
                    <div class="mx-2">
                        {{ $movie->movie_date}}
                       </div>
            </div>
            <div class="card-bot text-white d-flex justify-content-between align-items-center">

                <h4 class="card-title font-weight-bold text-uppercase my-3">

            {{ $hallname->Hall_name }}
                </h4>
                <button class="btn text-white btn-danger">

                    {{ $srctime }}

                </button>
              </div>
              <div class="option  my-3">
                {{-- <p class="lead text-white display-6"
                >You have selected seat

                <span id="r" class="text-white"></span>
                <span id="count" class="text-white"> </span>
              </p> --}}
              {{-- ticket selected row here --}}

              <div class="row-check d-flex my-3" style="width:100%">



              </div>

              <div class="payment">
                <div class="qty d-flex justify-content-between">
                   <p class="text-white"  >Quantity</p>
                   <p class="text-white" id="qty"></p>
                   <input type="text" name="qty" id="qq" value=1 hidden>
                </div>
                <div class="type d-flex justify-content-between">
                    <p class="text-white">Type</p>
                    <p class="text-white">Regular</p>
                 </div>
                 <div class="price d-flex justify-content-between">
                    <p class="text-white">Price</p>
                    <input type="text" name="price" id="pri" value={{  $seat->price }}  hidden>
                    <p class="text-white" class="price">${{  $seat->price }}</p>
                 </div>
                 <hr>
                 <div class="total d-flex justify-content-between">
                    <p class="text-white">Total</p>
                    <p class="amt text-white font-weight-bold"

 style="font-size: 25px" id="txtamt"></p>
         <input type="text" name="totalprice" id="amt" value=""  hidden>
                 </div>
              </div>

              <div class="confirm">
                <a href="/movie/payment" id ="press">
                    <button class="btn bg-danger text-white my-3" id="btn-continue" style="visibility: hidden">Continue</button>
                </a>
                </div>
            {{-- </form> --}}


              </div>

            </div>



        </div>

      </div>




          </section>




    </div>



</div>
</form>
@endsection

