{{-- @extends('page.layouts.master') --}}

<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <link href="{{ asset('/assets/css/Schedule.css') }}" rel="stylesheet">

    <!-- link icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link
        href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&family=Roboto+Condensed:ital,wght@0,300;0,700;1,400&display=swap"
        rel="stylesheet">
</head>
<style>
    body {
        margin: 0px;
        padding: 0px;

        scroll-behavior: smooth;
        font-family: 'Oswald', sans-serif;
        font-family: 'Roboto Condensed', sans-serif;
        background: #303841;
    }

    nav,
    .container-fluid {
        background-color:#3A4750;
        padding: 0px;
        margin-top: 0px;
    }
    .navbar-light .navbar-nav .nav-link {
    color: grey;

    }


    .navbar-light .navbar-nav .nav-link:hover{
        color: red;
        font-weight: 600;

        scale: 1.25;
        transition:500ms ease-in-out;
    }
</style>

<body>
    <div class="container-fluid sticky-top">
        <nav class="container sticky-top navbar navbar-expand-lg navbar-light justify-content-between
        ">
            <a class="navbar-brand text-white" href="/" style="font-weight: bold">Cinema Express</a>

                <button class="navbar-toggler   bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon " style="color:white">

                    </span>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup" >
                    <div class="navbar-nav">
                      <a class="nav-item nav-link " href="/">Home <span class="sr-only">(current)</span></a>

                                <a class="nav-item nav-link " href="/location/showtime">Cinema</a>


                            @if(session()->has('select'))
                      <a class="nav-item nav-link text-white" href="/movie/detail">Showtime</a>
                      @endif
                      @if(!session()->has('login'))

                      <a class="nav-item nav-link " href="/login">Login</a>
                      <a class="nav-item nav-link " href="/register">Register</a>
                      @endif


                    @if(session()->has('login')== True)

                      <form action="http://127.0.0.1:8000/logout" method="POST" >
                        @csrf

                      <a class="nav-item nav-link text-white">
                        <input type="submit" value="Logout" class="border-0 ">
                      </a>


                    </form>
                    @isset($role)



                    @if ($role == 'Admin')
                    <a class="nav-item nav-link text-white" href="/admin/dashboard">Admin dashboard</a>
                    @endif



                    @endif
                    @endisset
                    </div>
                  </div>






          </nav>
    </div>
    <div class="wrapper">




        <div class="container">
            <div class="Allmaincontent">



                <div class="maincontent-left">
                    <div class="mid">
                        <div class="mid-left">
                            <div class="mid-locate ">
                                <a href="/" class="text-decoration-none">

                                    <h4 class="text-danger text-uppercase " style="font-weight: bold">

                                        Movie Schedule</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                    <hr style="color:white;margin-bottom:30px">
                    <div class="date container text-white d-flex" >

                        @foreach ($mdate as $date)
                        {{-- {{$sldate }} --}}
                        @if ($sldate == $date->movie_date)
                                <a href="/movie/detail?date={{ $date->movie_date }}"
                                    class="text-white text-decoration-none">
                                    <p style="border-radius: 5px;

                        font-size: 20px;margin-right:10px;
                    "
                                        class="p-2 bg-danger">
                                        {{ date('d M', strtotime($date->movie_date)) }}


                                    </p>
                                </a>
                            @else
                                <p style="background-color:#3A4750;border-radius: 5px;

font-size: 20px;margin-right:10px;
                    "
                                    class="p-2">
                                    <a href="/movie/detail?date={{ $date->movie_date }}"
                                        class="text-white text-decoration-none">
                                        {{ date('d M', strtotime($date->movie_date)) }}


                                </p>
                                </a>
                            @endif
                        @endforeach


                        {{-- >
                    @foreach ($mdate as $date)
                    @if ($sldate == $date->movie_date)
                    <a href="/movie/detail?id={{  $movie->id }}&date={{ $date->movie_date }}"

                        class="text-white text-decoration-none"
                        >
                    <p style="border-radius: 5px;

                    font-size: 20px;
                    "

                    class="p-2 bg-danger"
                    >
                    {{ date("d M",strtotime($date->movie_date))}}


                    </p>
                </a>
                    @else

                    <p style="background-color:#3A4750;border-radius: 5px;

                    font-size: 20px;
                    "

                    class="p-2"
                    >
                    <a href="/movie/detail?id={{  $movie->id }}&date={{ $date->movie_date }}" class="text-white text-decoration-none">
                    {{ date("d M",strtotime($date->movie_date))}}


                    </p>
                </a>
                    @endif --}}
                        {{-- <p style="background-color:#3A4750;border-radius: 5px;

                    font-size: 20px;
                    "

                    class="p-2"
                    >
                    {{ date("d M",strtotime($date->movie_date))}}


                    </p> --}}
                        {{-- <div class="mx-2">

                    </div>
                    @endforeach --}}



                    </div>
                    @if($mdate->count() == 0)
                    <span  style="color:gainsboro;">
                        No Movie Date has been published
                    </span>

                    @endif
                    @foreach ($screentime as $screen)
                        <div class="m-5"></div>

                        <div class="content-detail">
                            <div class="main-detail">
                                <div class="maincontent-detail">
                                    <div class="detail-left">
                                        <img src="/images/{{ $screen->movie->file_path }}" alt="poster"
                                            style="width:100%">
                                    </div>
                                    <div class="detail-right">


                                        <div class="mid-locate ">
                                            <div class="bg-danger p-2 " style="font-weight:bold">
                                                Location: Cinema Express
                                               <span class="" style="font-weight:bold"> {{ $screen->hall->location->district }}</span>
                                                {{-- {{ $screen->hall->location->district }} --}}
                                                {{-- {{ $screen->hall_id}} --}}
                                                {{-- {{ $screen->district }} --}}

                                            </div>
                                        </div>

                                        <div class="text">
                                            <span style="font-weight: 600">
                                                {{ $screen->movie->title }}</span>


                                            Hall:
                                            {{ $screen->hall->Hall_name }}
                                            {{-- {{ $screen->Hall_name }} --}}
                                            {{-- {{ $screen->hall->Hall_name}} --}}
                                        </div>
                                        <div class="desc">
                                            <span class="fs-6">Duration:
                                                {{ $screen->movie->duration }}

                                            </span>
                                            <span class="fs-6">
                                                {{ $screen->movie->languages }}
                                            </span>
                                        </div>
                                        <span
                                        class="fs-6">{{ date('d M/ Y ', strtotime($screen->movie->movie_date)) }}</span>

                                        <div class="btn-Time">
                                            @foreach ($showtime as $sh)
                                                @if ($screen->hall_id == $sh->hall_id)
                                                    {{--  check hall and location to match the show date --}}
                                                    @if ($sldate == $sh->movie_date)
                                                        {{-- condition on the current movie date wwith the --}}

                                                        @if($sh->status == 0)
                                                        <a href="/movie/showtime?id={{ $screen->movie->id }}
                                                  &time={{ $sh->screentime }}&hallid={{ $screen->hall_id }}&sid={{ $sh->id }}

                                            "
                                                            class="text-decoration-none">

                                                            <div class="btn text-white  btn-danger" >


                                                                {{ $sh->screentime }}
                                                            </div>

                                                        </a>
                                                        @else
                                                        <div class="btn text-white btn-dark" style="margin-right:20px">


                                                            {{ $sh->screentime }}
                                                        </div>


                                                        @endif

                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    @endforeach




                </div>



            </div>
        </div>



    </div>



    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
        integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>

    <script src="./Schedule.js"></script>
    <script type="text/javascript" src="{{ URL::to('assets/js/Schedule.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ URL::to('assets/js/location.js') }}"></script> --}}


</body>




</html>
