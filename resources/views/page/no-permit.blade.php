<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body style="background-color: aliceblue">
    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-lg-9">
                <div class="card p-5 shadow">
                    <center>
                        <img
                        class="w-50"
                        src="https://cdn3d.iconscout.com/3d/premium/thumb/error-404-8808968-7122237.png?f=webp" alt="">
                    </center>

                    <h3 class="text-center my-2 ">404 Unauthorization Alert</h3>
                    <p class="lead text-center">You do not have the authority to be here, please redirect back to main page</p>
                    <a class="text-center text-decoration-none fs-4 text-danger" href="/">Back</a>
                </div>

            </div>
        </div>
    </div>

</body>
</html>
