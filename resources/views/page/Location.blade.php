<!doctype html>
<html lang="en">

<head>
    <title>Location</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <link href="{{ asset('/assets/css/Location.css') }}" rel="stylesheet">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link
        href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&family=Roboto+Condensed:ital,wght@0,300;0,700;1,400&display=swap"
        rel="stylesheet">

    <style>
        body {

            margin: 0px;
            padding: 0px;

            scroll-behavior: smooth;
            font-family: 'Oswald', sans-serif;
            font-family: 'Roboto Condensed', sans-serif;
            background: #303841;
        }

        .locatCinema {
            width: auto;
            /* margin-bottom: 8px;
             */
            height: 290px;
            color: #eeeeee;
            background-color: #3A4750;
            /* box-shadow: 1px 1px 10px rgba(245, 245, 245, 0.438); */
            padding: 20px;
            margin-bottom: 35px;
        }

        nav,
        .container-fluid {
            background-color:#3A4750;
            padding: 0px;
            margin-top: 0px;
        }
        .navbar-light .navbar-nav .nav-link {
    color: grey;

    }


    .navbar-light .navbar-nav .nav-link:hover{
        color: red;
        font-weight: 600;

        scale: 1.25;
        transition:500ms ease-in-out;
    }
    </style>
    <!-- link icon -->

</head>

<body>
    <div class="container-fluid sticky-top">
        <nav class="container sticky-top navbar navbar-expand-lg navbar-light justify-content-between
        ">
            <a class="navbar-brand text-white font-weight-bold" href="/">Cinema Express</a>

                <button class="navbar-toggler   bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon " style="color:white">

                    </span>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup" >
                    <div class="navbar-nav">
                      <a class="nav-item nav-link " href="/">Home <span class="sr-only">(current)</span></a>
                            @if(session()->has('select'))
                                <a class="nav-item nav-link text-white" href="/location/showtime">Cinema</a>

                            @endif

                      <a class="nav-item nav-link " href="/movie/detail">Showtime</a>
                      @if(!session()->has('login'))

                      <a class="nav-item nav-link " href="/login">Login</a>
                      <a class="nav-item nav-link " href="/register">Register</a>
                      @endif


                    @if(session()->has('login')== True)

                      <form action="http://127.0.0.1:8000/logout" method="POST" >
                        @csrf

                      <a class="nav-item nav-link text-white">
                        <input type="submit" value="Logout" class="border-0 ">
                      </a>


                    </form>
                    @isset($role)



                    @if ($role == 'Admin')
                    <a class="nav-item nav-link text-white" href="/admin/dashboard">Admin dashboard</a>
                    @endif



                    @endif
                    @endisset
                    </div>
                  </div>






          </nav>
    </div>
    <div class="wrapper">

        <div class="pic">
            <img src="./image/Rectangle 8.jpg" alt="">
        </div>

        <div class="container">
            <div class="mid">
                {{-- <div class="mid-left">
                <button class="btn  text-white bg-danger btn-md">
                 Cinema Location


                    </button>
            </div> --}}

            </div>

            <hr>

            <!-- For location -->
            <div class="row">
                @foreach ($locations as $location)
                    <div class="col col-lg-4">
                        <div class="locatCinema">
                            <div class="title">
                                {{-- <div>
                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M215.7 499.2C267 435 384 279.4 384 192C384 86 298 0 192 0S0 86 0 192c0 87.4 117 243 168.3 307.2c12.3 15.3 35.1 15.3 47.4 0zM192 128a64 64 0 1 1 0 128 64 64 0 1 1 0-128z"/></svg>
                        </div> --}}


                            </div>
                            <h4 class="fw-bold">Cinema Express {{ $location->district }}</h4>
                            <div class="CapDesc">
                                <a  href="/movie/detail?location={{  $location->id}}">
                                <div class="caption">
                                    <button class="btn btn-caption btn-danger btn-sm "
                                    type="submit"
                                    >Show Time</button>

                                    <button class="btn btn-caption btn-danger btn-sm">
                                        <a href="{{$location->map_url  }}" target="blank" class="text-decoration-none text-white">
                                       See in Google Map

                                    </a></button>

                                </div>
                            </a>
                            </div>
                            <div class="desc">
                                <div className="maindesc">
                                    <p> {{ $location->address }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>


            <!-- end location -->
        </div>





    </div>



    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
        integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>

    <script type="text/javascript" src="{{ URL::to('assets/js/scripts.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ URL::to('assets/js/location.js') }}"></script> --}}

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
        integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>

</body>




</html>
