{{-- @extends('page.layouts.master') --}}

<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <link href="{{ asset('/assets/css/Schedule.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- link icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link
        href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&family=Roboto+Condensed:ital,wght@0,300;0,700;1,400&display=swap"
        rel="stylesheet">
</head>
<style>
    body {
        margin: 0px;
        padding: 0px;

        scroll-behavior: smooth;
        font-family: 'Oswald', sans-serif;
        font-family: 'Roboto Condensed', sans-serif;
        background: #303841;
    }

    nav,
    .container-fluid {
        background-color: #3A4750;
        padding: 0px;
        margin-top: 0px;
    }
    .navbar-light .navbar-nav .nav-link {
    color: grey;

    }


    .navbar-light .navbar-nav .nav-link:hover{
        color: red;
        font-weight: 600;

        scale: 1.25;
        transition:500ms ease-in-out;
    }
</style>

<body>
    <div class="container-fluid sticky-top">
        <nav class="container sticky-top navbar navbar-expand-lg navbar-light justify-content-between
        ">
            <a class="navbar-brand text-white font-weight-bold" href="/">Cinema Express</a>

                <button class="navbar-toggler   bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon " style="color:white">

                    </span>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup" >
                    <div class="navbar-nav">
                      <a class="nav-item nav-link text-white active" href="/">Home <span class="sr-only">(current)</span></a>
                      <a class="nav-item nav-link " href="/location/showtime">Cinema</a>
                      <a class="nav-item nav-link " href="/movie/detail">Showtime</a>
                      @if(!session()->has('login'))

                      <a class="nav-item nav-link " href="/login">Login</a>
                      <a class="nav-item nav-link " href="/register">Register</a>
                      @endif


                    @if(session()->has('login')== True)

                      <form action="http://127.0.0.1:8000/logout" method="POST" >
                        @csrf

                      <a class="nav-item nav-link text-white">
                        <input type="submit" value="Logout" class="border-0 ">
                      </a>


                    </form>
                    @isset($role)



                    @if ($role == 'Admin')
                    <a class="nav-item nav-link text-white" href="/admin/dashboard">Admin dashboard</a>
                    @endif



                    @endif
                    @endisset
                    </div>
                  </div>






          </nav>
    </div>
    <div class="wrapper">




        <div class="container">
            <div class="Allmaincontent">



                <div class="maincontent-left">
                    <div class="mid">
                        <div class="mid-left">
                            <div class="mid-locate ">
                                <a href="/" class="text-decoration-none">

                                    <h4 class="text-danger text-uppercase font-weight-bold">

                                        Movie Schedule</h4>
                                </a>

                            </div>
                        </div>

                    </div>
                    <hr style="color:white;margin-bottom:30px">
                    <div class="date container text-white d-flex mb-5 p-0">
                        @foreach ($mdate as $date)
                            @if ($sldate == $date->movie_date)

                                <a href="/movie/detail?id={{ $movie->id }}&date={{ $date->movie_date }}"
                                    class="text-white text-decoration-none">
                                    <p style="border-radius: 5px;

                    font-size: 20px;margin-right:20px;
                    "
                                        class="p-2 bg-danger">
                                        {{ date('d M', strtotime($date->movie_date)) }}


                                    </p>
                                </a>
                            @else
                                <p style="background-color:#3A4750;border-radius: 5px;

font-size: 20px;margin-right:20px;
                    "
                                    class="p-2">
                                    <a href="/movie/detail?id={{ $movie->id }}&date={{ $date->movie_date }}"
                                        class="text-white text-decoration-none">
                                        {{ date('d M', strtotime($date->movie_date)) }}


                                </p>
                                </a>
                            @endif
                            {{-- <p style="background-color:#3A4750;border-radius: 5px;

                    font-size: 20px;
                    "

                    class="p-2"
                    >
                    {{ date("d M",strtotime($date->movie_date))}}


                    </p> --}}

                        @endforeach

                        @if($mdate->count() == 0)
                        <span  style="color:gainsboro;">
                            No Movie Date has been published
                        </span>

                        @endif

                    </div>

                    @foreach ($screentime as $screen)
                        <div class="m-5"></div>
                        <div class="content-detail">
                            <div class="main-detail">
                                <div class="maincontent-detail">
                                    <div class="detail-left">
                                        <img src="/images/{{ $movie->file_path }}" alt="poster" style="width:100%">
                                    </div>
                                    <div class="detail-right">


                                        <div class="mid-locate">
                                            <p class="bg-danger p-2 font-weight-bold">
                                                Location: Cinema Express
                                                {{ $screen->hall->location->district }}


                                            </p>
                                        </div>

                                        <div class="text">
                                            <span style="font-weight-bold" class="font-weight-bold">
                                                {{ $movie->title }}</span>


                                            Hall:
                                            {{ $screen->hall->Hall_name }}

                                        </div>
                                        <div class="desc">
                                            <span class="text-white fs-6">Duration:
                                                {{ $movie->duration }}

                                            </span>

                                            <span class="fs-6">    {{ $movie->languages }}</span>

                                        </div>


                                        <div class="btn-Time">


                                            @foreach ($showtime as $sh)
                                                @if ($screen->hall_id == $sh->hall_id)
                                                {{-- <p>{{$sh->id}} {{ $screen->id }}</p> --}}
                                                    {{--  check hall and location to match the show date --}}
                                                    {{-- @if ($screen->id == $sh->id) --}}
                                                    @if ($sldate == $sh->movie_date)
                                                        {{-- condition on the current movie date wwith the --}}




                                                            @if($movie->id  ==  $sh->movie->id)
                                                            @if($sh->status == 0)
                                                            {{-- movie is on going --}}
                                                            <a href="/movie/showtime?id={{ $movie->id }}
                                                                &time={{ $sh->screentime }}&hallid={{ $screen->hall_id }}&sid={{ $sh->id }}"
                                                                class="text-decoration-none">
                                                            <div class="btn btn-danger " style="margin-right:10px">

                                                                {{-- {{ $sh->movie->id}}
                                                                {{ $movie->id }} --}}

                                                                {{-- {{ $sh->status }} --}}

                                                                {{ $sh->screentime }}
                                                            </div>
                                                        </a>
                                                            @else




                                                            <div class="btn text-white" style="background-color: #3A4750">



                                                                {{ $sh->screentime }}
                                                            </div>
                                                            @endif
                                                            @endif
                                                            {{-- @endif --}}
                                                            {{-- @endif --}}

                                                    @endif
                                                @endif
                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    @endforeach
                    {{-- @if ($screentime->count() == 0)
                    <span class="mx-3"
                    style="color:gainsboro"

                    >There is no showtime for this movie yet</span>
                   @endif --}}

                </div>




                <div class="maincontent-right">
                    <img src="/images/{{ $movie->file_path }}" alt=""
                        style="width: 100%;height:auto;object-fit: cover">
                    <div class="Maininfor">
                        <div class="infor">
                            <h3 class="Miantitle pt-4 font-weight-bold text-left"> {{ $movie->title }}</h3>
                            <div class="body" style="color:white">
                                <h5>{{ $movie->genre }} | {{ $movie->duration }} | PG-13 | {{ $movie->movie_date }}</h5>
                                <h5 class="font-weight-bold">DIRECTOR: <span class="font-weight-light">{{ $movie->director }}</span></h5>
                                <h5 class="font-weight-bold">Writer: <span class="font-weight-light">Warner Bro</span></h5>
                                <h5 class="font-weight-bold">Languages: <span class="font-weight-light">{{ $movie->languages }}</span></h5>
                                <h5 class="font-weight-bold">Genre: <span class="font-weight-light">{{ $movie->genre }}</span></h5>
                                <h5 class="Moviedesc font-weight-bold">Movie Description: </h5>
                                <p class="fs-6" style="color:rgb(187, 187, 187)">Arthur Fleck, a party clown, leads an impoverished life with his ailing mother. However,
                                    when society shuns him and brands him as a freak, he decides to embrace the life of
                                    crime and chaos.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>



    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
        integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>

    <script src="./Schedule.js"></script>
    <script type="text/javascript" src="{{ URL::to('assets/js/Schedule.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ URL::to('assets/js/location.js') }}"></script> --}}


</body>




</html>
