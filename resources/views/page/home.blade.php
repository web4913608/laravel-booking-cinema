@extends('page.layouts.master')





@section('carol')
    <div id="myCarousel"class="carousel slide carousel-fade" data-ride="carousel" data-interval="2500">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://www.majorcineplex.com.kh/load_file/ads/file_20232613032656.jpg" class="d-block w-100"
                    alt="img-one" style="width: 100%;border:0px;border-bottom: 22px solid red">
            </div>
            <div class="carousel-item">
                <img src="https://www.majorcineplex.com.kh/load_file/ads/file_20235312105332.png" class="d-block w-100"
                    alt="img-two" style="width: 100%;border:0px;border-bottom: 22px solid red">
            </div>
            <div class="carousel-item" style="width: 100%;border:0px;border-bottom: 22px solid red">
                <img src="https://www.majorcineplex.com.kh/load_file/ads/file_20224020014014.png" class="d-block w-100"
                    alt="img-three">
            </div>
        </div>
        <br> <br> <br>
        <div class="three-dot ">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
        </div>


    </div>
@endsection
@section('content')
    <div class="row justify-content-between align-items-center gx-5">
        {{-- <div class="col-lg-3 col-md-6 ">

            <select class="custom-select">
                <option selected>Choose movie date</option>
                <option value="17/07/2023">17/07/2023</option>
                <option value="17/07/2023">19/07/2023</option>
                <option value="17/07/2023">20/07/2023</option>
            </select>

        </div> --}}
        <div class="col-lg-12 col-md-6 justify-content-center d-lg-flex d-md-block ">


            <form class="form w-100 d-flex" style="max-width: 600px" action="/search">
                <input class="form-control mr-sm-2 " type="search" placeholder="Search movie title" aria-label="Search"
                 name="title" >
                 <button class="btn btn-danger" type="submit">Search</button>

            </form>



        </div>

    </div>
    <br>
    <hr>
@endsection

@section('movie-section')
    <div class="row align-items-center gx-5">

        <div class="btn-groupcol-12">
        @if (     session()->get('now'))
        <button class="btn  text-white bg-danger">
            <a href="/" class="text-white text-decoration-none">Now Showing</a>
        @else
        <button class="btn  text-white ">
            <a href="/" class="text-white text-decoration-none">Now Showing</a>


            </button>
        @endif




        </div>

        <div class="btn-groupcol-12">
            @if (     session()->get('soon'))
            <button class="btn  text-white bg-danger">
                <a href="/comingsoon" class="text-white text-decoration-none">Coming Soon</a>
            @else
            <button class="btn  text-white ">
                <a href="/comingsoon" class="text-white text-decoration-none">Coming Soon</a>


                </button>
            @endif




            </div>
    </div>
@endsection

@section('move-row')

    <div class="row my-5">

        @foreach ($movies as $movie)

                <div class="col-lg-3">

                    @if ($movie->state == 'comingsoon')
                    <a href="/comingsoon" class="text-decoration-none">
                    @else
                    <a href="/movie/detail?id={{$movie->id}}" class="text-decoration-none">
                    @endif


                    <div class="card text-left b-0">
                        <img class="card-img-top " src="/images/{{ $movie->file_path }}" alt="poster">
                        <div class="card-body">
                            <h2 class="card-title lead ont-weight-500 text-uppercase">{{ $movie->title }}</h2>
                            <div class="card-inner card-text d-flex justify-content-between">
                                <div class="">
                                    Duration
                                </div>
                                <div class="">
                                    {{ $movie->duration }}
                                </div>
                                <div class="">
                                    {{ $movie->languages }}
                                </div>
                            </div>
                            <div class="card-bot text-white">
                                Date: {{ $movie->movie_date }}
                            </div>
                        </div>

                    </div>
                    <a href="/movie/detail">
                </div>


        @endforeach





    </div>
    @if ($movies->count() == 0)
    {{-- <img src="https://assets8.lottiefiles.com/packages/lf20_KOXhzYGQfb.json" alt="" class="fluid"> --}}
            <div class="notfound">
                <h5 class="text-white text-center font-weight-bold">No results found !!!
                </h5>


                <h6 class="text-white text-center">There is no movies up to date yet</h6>
            </div>

@endif
    {{-- <div class="row my-5">
        <div class="col-lg-3">

            <div class="card text-left b-0">
              <img class="card-img-top " src="https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg" alt="">
              <div class="card-body">
                <h2 class="card-title lead ont-weight-500 text-uppercase">Joker the movie</h2>
                <div class="card-inner card-text d-flex justify-content-between">
                        <div class="">
                            Duration
                        </div>
                        <div class="">
                          1h30min
                        </div>
                        <div class="">
                          3D/English
                        </div>
                </div>
                <div class="card-bot text-white">
                    Date: 21/05/2023
                  </div>
              </div>

            </div>
        </div>
        <div class="col-lg-3">

            <div class="card text-left b-0">
              <img class="card-img-top " src="https://m.media-amazon.com/images/I/71WiYBT2QsL._AC_UF894,1000_QL80_.jpg" alt="">
              <div class="card-body">
                <h2 class="card-title lead ont-weight-500 text-uppercase">Joker the movie</h2>
                <div class="card-inner card-text d-flex justify-content-between">
                        <div class="">
                            Duration
                        </div>
                        <div class="">
                          1h30min
                        </div>
                        <div class="">
                          3D/English
                        </div>
                </div>
                <div class="card-bot text-white">
                    Date: 21/05/2023
                  </div>
              </div>

            </div>
        </div>
        <div class="col-lg-3">

            <div class="card text-left b-0">
              <img class="card-img-top " src="https://m.media-amazon.com/images/M/MV5BMDdmMTBiNTYtMDIzNi00NGVlLWIzMDYtZTk3MTQ3NGQxZGEwXkEyXkFqcGdeQXVyMzMwOTU5MDk@._V1_FMjpg_UX1000_.jpg" alt="">
              <div class="card-body">
                <h2 class="card-title lead ont-weight-500 text-uppercase">Joker the movie</h2>
                <div class="card-inner card-text d-flex justify-content-between">
                        <div class="">
                            Duration
                        </div>
                        <div class="">
                          1h30min
                        </div>
                        <div class="">
                          3D/English
                        </div>
                </div>
                <div class="card-bot text-white">
                    Date: 21/05/2023
                  </div>
              </div>

            </div>
        </div>
        <div class="col-lg-3">

            <div class="card text-left b-0">
              <img class="card-img-top " src="https://i.insider.com/5ca3d2b892c8866e8b4618d9?width=750&format=jpeg&auto=webp" alt="">
              <div class="card-body">
                <h2 class="card-title lead ont-weight-500 text-uppercase">Joker the movie</h2>
                <div class="card-inner card-text d-flex justify-content-between">
                        <div class="">
                            Duration
                        </div>
                        <div class="">
                          1h30min
                        </div>
                        <div class="">
                          3D/English
                        </div>
                </div>
                <div class="card-bot text-white">
                    Date: 21/05/2023
                  </div>
              </div>

            </div>
        </div>


    </div> --}}
@endsection
