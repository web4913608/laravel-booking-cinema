@extends('page.layouts.master')

@section('payment')
<div class="row align-items-center gx-5 my-5">

    <div class="btn-groupcol-12">
        <button class="btn  text-white " >Select Seat</button>
        <button class="btn  text-white " id="selected">Reserve / Buy</button>
        <button class="btn  text-white ">Payment</button>
    </div>


</div>
<hr>
<form action="/payment" method="POST">
    @csrf
<div class="row " style="margin-top:50px">
    <div class="col-lg-6">
        <div class="card-body p-5" style="background-color: #222426" >

            <h2 class="card-title font-weight-500 text-uppercase">{{ $movie->title }}</h2>
            <div class="card-inner card-text d-flex ">
                    <div class="">
                       Action
                    </div>
                    <div class="bar mx-1">
                        |
                    </div>

                    <div class="mx-1">
                    {{ $movie->duration }}
                    </div>
                    <div class="bar mx-1">
                        |
                    </div>
                    <div class="mx-2">
                     PG-13
                    </div>
                    <div class="bar mx-1">
                        |
                    </div>
                    <div class="mx-2">
                        {{ $movie->movie_date }}
                       </div>
            </div>
            <div class="card-bot text-white d-flex justify-content-between align-items-center">

                <h4 class="card-title font-weight-bold text-uppercase my-3">

                 {{     $show->hall->Hall_name }}


                </h4>
                <button class="btn text-white btn-danger">
                    {{     $show->screentime}}

                </button>
              </div>
              <div class="option  my-3">

              {{-- ticket selected row here --}}
              <div class="payment">
                <div class="qty d-flex justify-content-between">
                   <p class="text-white">Selected Seats</p>

                   <div class="text-white">
                    @foreach ($seatrow as $st)
                    <button class="btn btn-danger btn-sm" disabled>
                        {{
                            $st->row
                        }}{{
                            $st->number
                        }}
                    </button>
                    @endforeach
                </div>


                      <input type="text" name="qty" value = {{      $qty }} hidden>
                </div>
              <div class="payment">
                <div class="qty d-flex justify-content-between">
                   <p class="text-white">Quantity</p>
                    <p class="text-white">x {{
                        $qty
                    }}</p>
                      <input type="text" name="qty" value = {{      $qty }} hidden>
                </div>
                <div class="type d-flex justify-content-between">
                    <p class="text-white">Type</p>
                    <p class="text-white">Regular</p>
                 </div>
                 <div class="price d-flex justify-content-between">
                    <p class="text-white">Price</p>
                    {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                    <input type="text" name="price" value = {{      $price }} hidden>
                    <p class="text-white">$ {{
                        $price
                    }}</p>
                 </div>
                 <hr>
                 <div class="total d-flex justify-content-between">
                    <p class="text-white">Total</p>
                    <input type="text" name="total" value = {{    $amt }} hidden>
                    <p class="text-white font-weight-bold" style="font-size: 25px">

                       $ {{
                            $amt
                        }}

                    </p>
                 </div>
              </div>
              <div class="payment-select">
                <label for="pay"> Payment Method</label>
                <a href="/checkout">Purchase with mastercard</a>
                <select class="custom-select" name="method" id="pay">
                    {{-- <option selected>Choose method to pay</option> --}}
                    <option value="Payment">Purchase with mastercard</option>
                    <option value="Reserved" selected>Booking or Reserved</option>
                    {{-- <option value="stripe payment">Stripe Payment</option> --}}
                  </select>
              </div>

              <div class="confirm my-3">

                    <button class="btn bg-danger text-white my-3 w-100" type="submit" id="online">Proceed to checkout</button>
              </div>
              <div class="confirm my-3">

                <a href="/checkout">

                <button class="btn bg-danger text-white my-3 w-100" type="button" id="mastercard" style="display:none">Check out</button>
                </a>
          </div>
            </form>


              </div>

            </div>





    </div>

</div>
<div class="margin" style="margin-bottom:300px">
</form>
</div>
@endsection
