<!DOCTYPE html>
<html>
   <head>
      <title>Stripe Payment</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   </head>
   <style>
       body{
        margin: 0px;
        padding: 0px;

        scroll-behavior: smooth;
        font-family: 'Oswald', sans-serif;
            font-family: 'Roboto Condensed', sans-serif;
        background: #303841;
    }
   </style>
   <body style="background-color:#303841 ">
      <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form action="/session" method="POST">
                    <div class="card my-5">
                        <div class="card-body">
                            <p class="lead">Please verify this link to continue</p>
                            <hr>
                            <p class="fs-6 " style="color:gray;">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem possimus animi, doloribus architecto accusamus eius libero modi delectus voluptatem cupiditate sint beatae iure officiis nam omnis voluptates! Cumque, deleniti dolorem!</p>
                                <a href="{{ url('/') }}" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Continue Shopping</a>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">


                            <button class="btn btn-success mx-3" type="submit" id="checkout-live-button"><i class="fa fa-money"></i> Checkout</button>
                        </div>
                    </div>




                    </form>
            </div>

        </div>


      </div>
   </body>
</html>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
  var $form = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form = $(".require-validation"),
    inputSelector = ['input[type=email]', 'input[type=password]', 'input[type=text]', 'input[type=file]', 'textarea'].join(', '),
    $inputs = $form.find('.required').find(inputSelector),
    $errorMessage = $form.find('div.error'),
    valid = true;
    $errorMessage.addClass('hide');
    $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
        var $input = $(el);
        if ($input.val() === '') {
            $input.parent().addClass('has-error');
            $errorMessage.removeClass('hide');
            e.preventDefault();
        }
    });
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  });

  function stripeResponseHandler(status, response) {
      if (response.error) {
          $('.error')
              .removeClass('hide')
              .find('.alert')
              .text(response.error.message);
      } else {
          /* token contains id, last4, and card type */
          var token = response['id'];
          $form.find('input[type=text]').empty();
          $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
          $form.get(0).submit();
      }
  }
});
</script>
