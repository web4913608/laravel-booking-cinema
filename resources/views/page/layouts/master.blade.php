<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

<meta charset="utf-8">

<title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/Location.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link rel="preconnect" href="https://fonts.googleapis.com">

<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&family=Roboto+Condensed:ital,wght@0,300;0,700;1,400&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('assets/css/navi.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/movieblock.css') }}">

{{-- <link rel="stylesheet" href="{{ asset('assets/js/seat.js') }}"> --}}

</head>
<style>
    body{
        margin: 0px;
        padding: 0px;

        scroll-behavior: smooth;
        font-family: 'Oswald', sans-serif;
            font-family: 'Roboto Condensed', sans-serif;
        background: #303841;
    }
    nav.navbar{


    }

  div.container-fluid{
         background: #303841;
        padding: 0px;
        margin-top:0px;
    }
    .navbar-nav > a{
        font-weight: 400;
    }
    .navbar-brand{
        font-weight: 500;
    }
    .navbar-nav > a{
        margin: 0px 10px;
    }
    footer{
        background-color: #303841;
        /* position: absolute;
        bottom: 0px; */
        width: 100%;
    }
    .navbar-light .navbar-toggler-icon {

    }

    .carousel-item img{


              height: auto;

/*
        object-fit: cover; */

    }
    .navbar-nav .nav-item{
     color: white;
    }
    .navbar-nav > a:hover{

    }
    .navbar-light .navbar-nav .nav-link {
    color: grey;

    }


    .navbar-light .navbar-nav .nav-link:hover{
        color: red;
        font-weight: 600;

        scale: 1.25;
        transition:500ms ease-in-out;
    }

    .input-icons i {
            position: absolute;
        }

        .input-icons {
            width: 100%;

        }

        .icon {
            padding: 10px;
            min-width: 40px;
        }

        .input-field {
            width: 100%;
            padding: 10px;
            text-align: center;
        }

    @media only screen and (max-width: 991px) {
    body {

    }

}
</style>
<body>
    <div class="container-fluid sticky-top">
        <nav class="container sticky-top navbar navbar-expand-lg navbar-light justify-content-between
        ">
            <a class="navbar-brand text-white font-weight-bold" href="/">Cinema Express</a>

                <button class="navbar-toggler   bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon " style="color:white">

                    </span>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup" >
                    <div class="navbar-nav">
                      <a class="nav-item nav-link text-white active" href="/">Home <span class="sr-only">(current)</span></a>
                      <a class="nav-item nav-link " href="/location/showtime">Cinema</a>
                      <a class="nav-item nav-link " href="/movie/detail">Showtime</a>
                      @if(!session()->has('login'))

                      <a class="nav-item nav-link " href="/login">Login</a>
                      <a class="nav-item nav-link " href="/register">Register</a>
                      @endif


                    @if(session()->has('login')== True)

                      <form action="http://127.0.0.1:8000/logout" method="POST" >
                        @csrf

                      <a class="nav-item nav-link text-white">
                        <input type="submit" value="Logout" class="border-0 ">
                      </a>


                    </form>
                    @isset($role)



                    @if ($role == 'Admin')
                    <a class="nav-item nav-link text-white" href="/admin/dashboard">Admin dashboard</a>
                    @endif



                    @endif
                    @endisset
                    </div>
                  </div>






          </nav>
    </div>

    <section class="container-fluid p-0">
        @yield('carol')
    </section>



      <div class="container">

        @yield('content')
        @yield('movie-section')
        @yield('move-row')
        @yield('location')

        @yield('schedule')
    </div>
    <section class="page-detail container">
        @yield('movie-detail')
    </section>

    <section class="payment">
        <div class="container text-white">
            @yield('payment')
        </div>



    </section>
    <section class="admin">
        <div class="container">
            @yield('admin')
        </div>



    </section>
    <div class="container">
        @yield('location')
    </div>







    <footer class="text-center text-white">
        <!-- Grid container -->
        <div class="container pt-4">
          <!-- Section: Social media -->
          <section class="mb-4">
            <!-- Facebook -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-facebook-f"></i
            ></a>

            <!-- Twitter -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-twitter"></i
            ></a>

            <!-- Google -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-google"></i
            ></a>

            <!-- Instagram -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-instagram"></i
            ></a>

            <!-- Linkedin -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-linkedin"></i
            ></a>
            <!-- Github -->
            <a
              class="btn btn-link btn-floating btn-lg text-dark m-1"
              href="#!"
              role="button"
              data-mdb-ripple-color="dark"
              ><i class="fab fa-github"></i
            ></a>
          </section>
          <!-- Section: Social media -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);" style="text-color:grey">
          © 2023 Copyright:
          <a class="text-white" href="https://mdbootstrap.com/">Panhavorn</a>
        </div>
        <!-- Copyright -->
      </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ URL::to('assets/js/seat.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ URL::to('assets/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('assets/js/location.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>

</body>
</html>
