<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">


</head>
<style>
    body {
        margin: 0px;
        padding: 0px;
        scroll-behavior: smooth;
        font-family: 'Oswald', sans-serif;
        font-family: 'Roboto Condensed', sans-serif;
        background: #303841;
    }
    #outer_box {
      position: relative;
      border: 2px solid black;
      width: 500px;
      height:300px;
  }

  #yellow_highlight {
      position: absolute;
      width: 0px;
      height: 30px;
      border-left: 300px;
      border-color: yellow;
      border-style: solid;
      top: 0;
      left: 0px
  }

  #message_text {
      position: absolute;
      top: 0;
      left: 0px;
  }
</style>

<body>

    <div class="container" style="margin-top: 200px;">

        <!-- absolute path -->
        <div class="row align-items-center justify-content-center gx-5 my-5">
            <div class="col-lg-6">
                <div class="btn-groupcol-12">
                    <button class="btn  text-white ">Select Seat</button>
                    <button class="btn  text-white " id="selected">Reserve / Buy</button>
                    <button class="btn  text-white btn-danger   ">Payment</button>
                </div>
            </div>



        </div>
        <div class="row justify-content-center" style="margin-top:50px">

            <div class="col-lg-6">

                <div class="card-body p-5" style="background-color: #222426">
                    <div class="top d-flex justify-content-between">
                        <h3 class="card-title text-white font-weight-bold text-uppercase" style="font-weight: bold">
                            {{-- {{ $movie->title }} --}}
                          Order Detail

                        </h3>
                        <a class="text-white" href="/">Home</a>
                        <button class="text-white bg-danger btn" style="font-weight: bold" id="btnprint">
                            {{-- {{ $movie->title }} --}}
                         print

                        </button>
                    </div>

                    <div class="card-inner card-text d-flex ">
                        <div class="">
                            Action
                        </div>
                        <div class="bar mx-1">
                            |
                        </div>

                        <div class="mx-1">
                            {{-- {{ $movie->duration }} --}}
                        </div>
                        <div class="bar mx-1">
                            |
                        </div>
                        <div class="mx-2">
                            PG-13
                        </div>
                        <div class="bar mx-1">
                            |
                        </div>
                        <div class="mx-2">
                            {{-- {{ $movie->movie_date }} --}}
                        </div>
                    </div>
                    <div class="card-bot text-white d-flex justify-content-between align-items-center">


                    </div>
                    <div class="price d-flex justify-content-between">
                        <p class="text-white">Movie Title</p>
                        {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                        {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                        <p class="text-white"> {{

                                $showtime->movie->title
                        }}</p>
                    </div>
                    <div class="price d-flex justify-content-between">
                        <p class="text-white">Customer name</p>
                        {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                        {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                        <p class="text-white"> {{ $customername }}</p>
                    </div>
                    <div class="price d-flex justify-content-between">
                        <p class="text-white">Customer email</p>
                        {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                        {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                        <p class="text-white"> {{ $customeremail }}</p>
                    </div>
                    <div class="price d-flex justify-content-between">
                        <p class="text-white">Order Date</p>
                        {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                        {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                        <p class="text-white"> {{ date_format($reserve->created_at, 'm/d/Y h:i:s') }}</p>
                    </div>
                    <div class="price d-flex justify-content-between">
                        <p class="text-white">Booking Ticket</p>
                        {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                        {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                        <p class="text-white bg-danger p-1">
                            {{ $payment->booking->booking_ticket }}
                        </p>
                    </div>
                    <div class="option  my-3">

                        {{-- ticket selected row here --}}
                        <div class="payment">

                            <hr class="text-white">
                            <div class="qty d-flex justify-content-between">
                                <p class="text-white">Seats</p>

                                <div class="text-white">
                                    @foreach ($seatrow as $seat)
                                        {{ $seat->row }}
                                        {{ $seat->number }}
                                    @endforeach
                                </div>


                                {{-- <input type="text" name="qty" value = {{      $qty }} hidden> --}}
                            </div>
                            <div class="payment">
                                <div class="qty d-flex justify-content-between">
                                    <p class="text-white">screen time</p>

                                    <p class="text-white">
                                        {{ $showtime->screentime }}
                                    </p>



                                </div>
                                <div class="qty d-flex justify-content-between">
                                    <p class="text-white">Halls</p>

                                    <p class="text-white">
                                        {{ $showtime->hall->Hall_name }}
                                    </p>



                                </div>
                                <div class="qty d-flex justify-content-between">
                                    <p class="text-white">Ticket(s)</p>
                                    <p class="text-white">x
                                        {{ $reserve->qty }}</p>
                                    {{-- <input type="text" name="qty" value = {{      $qty }} hidden> --}}
                                </div>


                                <div class="price d-flex justify-content-between">
                                    <p class="text-white">Price</p>
                                    {{-- <input type="text" name="seats" id="" value={{ $seats }}> --}}
                                    {{-- <input type="text" name="price" value = {{      $price }} hidden> --}}
                                    <p class="text-white">$ {{ $reserve->price }}</p>
                                </div>
                                <div class="qty d-flex justify-content-between">
                                    <p class="text-white">Discount</p>
                                    <p class="text-white">
                                        % {{ $payment->discount }}
                                    </p>

                                </div>


                                <div class="total d-flex justify-content-between">
                                    <p class="text-white">Total amount</p>
                                    {{-- <input type="text" name="total" value = {{    $amt }} hidden> --}}
                                    <p class="text-white font-weight-bold" style="font-size: 25px">

                                        $ {{ $payment->amount }}

                                    </p>
                                </div>
                            </div>
                            <div class="payment-select">


                            </div>

                            {{-- <div class="confirm my-3">
                                    <button class="btn bg-danger text-white my-3 w-100" type="submit">Proceed to checkout</button>
                              </div> --}}
                            </form>


                        </div>

                    </div>





                </div>

            </div>


        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
        </script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>
        <script type="text/javascript" src="{{ URL::to('assets/js/scripts.js') }}"></script>
        <script type="text/javascript" src="{{ URL::to('assets/js/duration.js') }}"></script>
        <script type="text/javascript" src="{{ URL::to('assets/js/payment.js') }}"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>

</body>

</html>
