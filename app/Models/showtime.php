<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class showtime extends Model
{
    use HasFactory;

    protected $table='_showtime';
    public $timestamps = true;
    protected $fillable = [
       'movie_id','screentime','hall_id',
    'movie_date','status','trash'
    ];
    public function movie(){
        return $this->belongsTo(Movie::class);
    }
    // public function st(){
    //     return $this->hasOne(showtime::class);
    // }

    public function hall(){
        return $this->belongsTo(Hall::class);
    }

    public function show(){
        return $this->hasMany(showtime::class);
    }
    // public function shtime(){
    //     return $this->hasMany(showtime::class);
    // }
}
