<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class seat extends Model
{
    use HasFactory;
    protected $table='seat';
    public $timestamps = true;
    protected $fillable = [
        's_id', 'row','number','active'
    ];
    public function hall(){
        return $this->belongsTo(Hall::class);
    }
    public function st(){
        return $this->belongsTo(showtime::class);
    }
    // public function location(){
    //     return $this->belongsTo(Location::class);
    // }
}
