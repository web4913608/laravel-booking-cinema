<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $table = 'reservations';
    protected $fillable=[
        'price',
        'qty',
        'user_id',
        'seats',
        'show_id',
        'booking_ticket',
        'paid'
    ];
    protected $casts =[
        'seats'=>'array'
    ];
    public function show(){
        return $this->belongsTo(showtime::class);
    }

    public function booking(){
        return $this->hasMany(Reservation::class);
    }
    // public function shtime(){
    //     return $this->belongsTo(showtime::class);
    // }
    public function users(){
        return $this->belongsTo(User::class);
    }

}
