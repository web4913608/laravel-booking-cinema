<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    use HasFactory;
    protected $table='hall_tables';
    public $timestamp = true;
    protected $fillable = ['Hall_name','location_id'];
    public function hall(){
        return $this->hasOne(Hall::class);
    }
    
    public function location(){
        return $this->belongsTo(Location::class);
    }

}
