<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $timestamp = true;
    protected $fillable=[
        'method',
        'discount',
        'amount',
        'booking_id'

    ];
    public function booking(){
        return $this->belongsTo(Reservation::class);
    }

}
