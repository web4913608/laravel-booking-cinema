<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Movie extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'movies';
    protected $fillable =['title','director','description','duration','movie_date','state','genre','file_path'
    ,'languages','status'


];
public function movie(){
    return $this->hasMany(Movie::class);
}
}
