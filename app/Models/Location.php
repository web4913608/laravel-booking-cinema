<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    protected $table = 'locations';
    public $timestamps = true;
    protected $fillable = [
        'city','address','district','map_url','updated_at','created_at'
    ];
    public function location(){
        return $this->hasMany(Location::class);
    }
}
