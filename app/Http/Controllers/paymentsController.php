<?php

namespace App\Http\Controllers;

use App\Mail\BookingTicket;
use App\Models\Payment;
use App\Models\Reservation;
use App\Models\seat;
use App\Models\showtime;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class paymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $p = Payment::all();
        return view("admin.payment.payment")->with([
            'p'=>   $p
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $reserve = new Reservation;
        $reserve->price =  request('price');
        $reserve->qty =  request('qty');
        $selectedseat = session()->get('seats');



        // return $selectedseat;
        $sid = array_map('intval',  $selectedseat );
        $reserve->show_id =   session()->get('sid');
        $reserve->booking_ticket = Str::random(8);
        // return $sid;
        $reserve->seats = $sid;

        $arruser =  session()->get('uid');
        // return session()->get('uid');
        // return $arruser;

        $reserve->user_id =      $arruser;



        // return       $arruser[0];
        // return session()->get('uid');
        // return      $request->all();
        $reserve->save();

        $booking_id = $reserve->id; //get the inserted id back


        //  session()->push('booking_id',       $booking_id);

        $total = request('price') *   request('qty');


        $method = $request->input('method');
        $dis = 0;
        // return $method;

        //insert to payment table also
        $amt =   $total;
        $payment = new Payment;
        $payment->method = $method;
        $payment->discount =     $dis ;
        $payment->amount =     $total ;
        $payment->booking_id =        $booking_id ;

        $payment->save();
        session()->put('booking_id',$payment->id);


        //create session order id



       seat::whereIn('id',  $sid )->update([
           'active' => false
        ]);

        return   redirect('/payment/success');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


        if(!session()->get('booking_id')){
            return view('page.no-permit');
        }
        // $bid = session()->get('booking_id');

       $p=Payment::where('id',    $id)->first();
    //    return $p;

        $booking = Reservation::where('id',      $p->booking_id
        )->first();




        $users = User::where('id',$booking->user_id)->first();
        $customername = $users->name;
        $customeremail = $users->email;
        // return  $users;

        // return  $booking;
        $showtime =  showtime::where('id',   $booking->show_id )->first();
        // return $showtime;

        $seats = $booking->seats;

        $sid = array_map('intval',      $seats );
        $seatrow = seat::whereIn('id',    $sid  )->get();

        // return         $seatrow ;
        return   view("page.success")->with([
            'payment'=>$p,
            'reserve'=>    $booking,
            'showtime'=>        $showtime,
            'seatrow' =>    $seatrow,
            'customername'=>$customername,
            'customeremail'=>        $customeremail
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
