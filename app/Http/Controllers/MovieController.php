<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $movies =  Movie::all();
        return view('admin.movie.movie',['Movies'=>$movies]);

    }

    public function search(Request $request)
    {
        //

        if(request('title')){
            $movies =  Movie::where('title','LIKE',"%".request('title')."%")->get();
        }
        else{
            $movies =  Movie::all();
            return view('admin.movie.movie',['Movies'=>$movies]);
        }

        return view('admin.movie.movie',['Movies'=>$movies]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.movie.createmovie');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $input = $request->input();
         $file = $request->file('file_path');

        if($request->file('file_path')){
            $filename = $request->file('file_path')->getClientOriginalName();
            //store filename in public folder
            $file->move('images',$filename);
            //insert filename to database
            $input['status']='on-going';
            $input['file_path'] =        $filename;



        }

        Movie::create(     $input );
        return redirect('/movies');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        //view detail movie
       $movie= Movie::where('id','=',$id)->get();
        return view('admin.movie.editmovie',['movie'=>$movie]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //
        $movie= Movie::find($id);
        return view('admin.movie.editmovie')->with(['movie'=>$movie]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // $movie = Movie::class;
        $input = $request->input();

       if($request->file('file_path')){

             $movie= Movie::find($id);


            //search the file path in database
           $desination = 'images'.       $movie->file_path;
           if(File::exists(  $desination )){
            File::delete(     $desination );
            //if the file path contain in images folder delete them

           }
           $file = $request->file('file_path');
           //replace new file path to the public/images folder
            $filename = $request->file('file_path')->getClientOriginalName();
           //store filename in public folder
           $file->move(   'images',$filename);
           //insert filename to database
           $input['status'] = $request->input('status');
           $input['file_path'] =        $filename;



       }

        // return $request->file('file_path');
        Movie::find($id)->update(      $input);
        return redirect('/movies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Movie::find($id)->delete();
        return redirect('/movies');
    }
}
