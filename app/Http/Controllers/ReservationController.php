<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\seat;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reserve = Reservation::orderBy('id','Desc')->get();
        // $selectseat = $reserve->seats;
        $re = [];

        // return $reserve;

        // foreach (   $reserve as $reserves) {

        //     array_push($re,$reserves->seats);
        // }
        // return $re;
        $bookedseat = seat::all();
        // return $bookedseat;

        // return $bookedseat;

        // return    $reserve;

        // return $reserve;
        return view("admin.booking.booking")->with([
            'reserve'=>   $reserve,
            'bs'=>      $bookedseat

        ]);
    }

    public function search(Request $request){
        // $search =request('ticket');
        $reserve = Reservation::where('booking_ticket',)->orderBy('id','Desc')->get();
        // $selectseat = $reserve->seats;
        $re = [];
        if(request('searchticket')){
            $search = request('searchticket');
            // return $search;
            if(   $search ==''){
                $re = Reservation::where('booking_ticket',)->orderBy('id','Desc')->get();
            }
            else{
                $re = Reservation::where("booking_ticket","LIKE",$search)->orderBy('id','Desc')->get();
            }
            // return $search;
            // $movie = Movie::where('title','LIKE',"%".      $title."%")->orderBy('id','Desc')->get();


            // return        $re;
        }
        else{
            // $re = Reservation::where('booking_ticket',)->orderBy('id','Desc')->get();
            return redirect('/booking');
        }

        $bookedseat = seat::all();

        return view("admin.booking.booking")->with([
            'reserve'=>        $re,
            'bs'=>      $bookedseat

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $re = Reservation::where('id',$id)->first();
        // return $re;


        return view("admin.booking.bookingedit")->with('reserve',$re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Reservation::where('id',$id)->update([
            'paid'=>$request->input('paid')
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
        return redirect()->back();
    }

}
