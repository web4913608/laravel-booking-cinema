<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\BookingTicket;

class MailController extends Controller
{
    //
    public function sendmail(){
        Mail::to('Nightpp19@gmail.com')->send(new BookingTicket());

    }
}
