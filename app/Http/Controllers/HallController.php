<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Location;
use Illuminate\Http\Request;

class HallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hall = Hall::all();
        return view("admin.halls.hall")->with(['halls'=>  $hall]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $hall = Location::all();
        return view("admin.halls.hallcreate")->with(['locations'=>  $hall]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        Hall::create(     $request->input());
        return redirect('/halls');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hall  $hall
     * @return \Illuminate\Http\Response
     */
    public function show(Hall $hall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hall  $hall
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $location = Location::all();
        $hall=Hall::find($id);
        return view("admin.halls.halledit")
        ->with(['hall'=>$hall,'locations'=>$location]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hall  $hall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $hall = Hall::find($id)->update($request->input());
        return redirect('halls');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hall  $hall
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Hall::find($id)->delete();
        return redirect('halls');

    }
}
