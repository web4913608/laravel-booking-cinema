<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Reservation;
use App\Models\seat;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Stripe\Stripe;

class StripeController extends Controller
{
    //
    public function index(){
        return view("page.home");
    }

    public function checkout(){

        return view("page.stripes.stripe");

    }

    public function session(Request $request){
        \Stripe\Stripe::setApiKey(config('stripe.sk'));

        $price = $request->session()->get('prices');

        $moviename = $request->session()->get('moviename');
        $qty = session()->get('qty');
        $totalprice =$request->session()->get('amt');
        $session = \Stripe\Checkout\Session::create([
            'line_items'  => [
                [
                    'price_data' => [
                        'currency'     => 'USD',
                        'product_data' => [
                            "name" =>  'Ticket:'.$moviename,
                        ],
                        'unit_amount'  =>        $price  *100,
                    ],
                    'quantity'   =>     $qty,
                ],

            ],
            'mode'        => 'payment',
            'success_url' => route('success'),
            'cancel_url'  => route('checkout'),
        ]);
    return redirect()->away(   $session->url);
    }

    public function success(Request $request){
        $reserve = new Reservation;
        $reserve->price = session()->get('prices');
        $reserve->qty = session()->get('qty');
        $selectedseat = session()->get('seats');
        $reserve->paid = 1;


        // return $selectedseat;
        $sid = array_map('intval',  $selectedseat );
        $reserve->show_id =   session()->get('sid');
        $reserve->booking_ticket = Str::random(8);
        // return $sid;
        $reserve->seats = $sid;

        $arruser =  session()->get('uid');

        // return $arruser;

        $reserve->user_id =      $arruser;



        // return       $arruser[0];
        // return session()->get('uid');
        // return      $request->all();
        $reserve->save();

        $booking_id = $reserve->id; //get the inserted id back


        //  session()->push('booking_id',       $booking_id);

        $total = session()->get('prices') *  session()->get('qty');


        $method = 'Stripe Payment';
        $dis = 0;
        // return $method;

        //insert to payment table also
        $amt =   $total;
        $payment = new Payment;
        $payment->method = $method;
        $payment->discount =     $dis ;
        $payment->amount =     $total ;
        $payment->booking_id =        $booking_id ;

        $payment->save();
        session()->put('booking_id',$payment->id);


        //create session order id



       seat::whereIn('id',  $sid )->update([
           'active' => false
        ]);

        return   redirect('/payment/success');
        // return "Thanks you for your order, You have completed your online purchase";
    }
}

