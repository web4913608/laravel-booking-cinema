<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Movie;
use App\Models\Reservation;
use App\Models\seat;
use App\Models\showtime;
use Illuminate\Http\Request;

class ShowtimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Showtime=showtime::orderby('id','DESC')->get();

        // $Showtime= showtime::orderBy('row', 'asc')->get();


        return view("admin.screeningtime.screentime")
        ->with(['showtimes'=>          $Showtime,



    ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $hall=Hall::all();
        $movie=Movie::all();
        return view("admin.screeningtime.createscreentime",[
            'halls' =>      $hall,
            'movies' =>$movie
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sid= showtime::create($request->all());

       $id = $sid->hall_id;
       $h = Hall::where('id',$id)->first();
       $request->session()->put('halls',$h);

       return redirect('/seats/create');
       return redirect('/showtime');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\showtime  $showtime
     * @return \Illuminate\Http\Response
     */
    public function show(showtime $showtime)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\showtime  $showtime
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $s = showtime::where('id',$id)->first();
        $hall=Hall::all();
        // return $s;
        $movie=Movie::all();
        return view("admin.screeningtime.editscreentime",[
            'halls' =>      $hall,
            'movies' =>$movie,
            'showtime'=>$s
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\showtime  $showtime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        showtime::find($id)->update($request->input());
        return redirect('/showtime');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\showtime  $showtime
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $s = showtime::find($id);
        // seat::where('i')
        // return    $s->id;
        $r = Reservation::where('show_id',1)->first();

        if($r){
            // return    $r;
            showtime::where('id',$id)->update([
                'trash'=>1
            ]);

        }
        else{
            seat::where('s_id', '=',$s->id)->delete();
            showtime::find($id)->delete();
        }





        return redirect('/showtime');
    }
    public function search(Request  $request)
    {
        //
        if(request('title')){

            $movieid =Movie::select('id')->where('title','LIKE',"%".request('title')."%")->get();
        }
        else{
            return redirect('/showtime');
        }
        // return   $movieid;
        $mid =[];
        foreach( $movieid  as $m){
          array_push(    $mid,$m->id);
        }

        $Showtime=showtime::whereIn('movie_id',   $mid)->orderby('id','DESC')->get();

        // $Showtime= showtime::orderBy('row', 'asc')->get();

        // return       $Showtime ;
        return view("admin.screeningtime.screentime")
        ->with(['showtimes'=>          $Showtime,



    ]);
    }
}
