<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Location;
use App\Models\seat;
use App\Models\showtime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $seat =seat::all();
        // $seat  seat::all()with('posts.comments')->get();
//         $seat =seat::all();
// //
//         $seat  =seat::with('hall.location')->get();
        // $seat = seat::orderBy('row', 'asc')->get();

        // return $seat;
        $seat = [];
        $locations = Location::all();

        if(session()->has('hid')){
            $hid = session()->get('hid');

            $showtime = showtime::where('hall_id',    $hid )->get();
        // return $showtime;
            return view("admin.seats.seat")->with(
                [
                    'seats'=>  $seat,
                'locations'=>        $locations,
                'showtimes' =>     $showtime


            ]);
        }

        if(session()->has('srt')){
            $seat = DB::select(
                DB::raw("select

                s.id,h.Hall_name,l.district,s.row,s.number,t.movie_date,t.screentime,s.active from seat s inner join _showtime t
                on s.s_id = t.id
                inner join hall_tables h
                on t.hall_id = h.id
                INNER JOIN locations l on h.location_id = l.id

                where t.screentime =
                '".session()->get('srt')."'
                order by s.row asc;"
          ));
        //   return $seat;

        }
    //   return session()->has('srt');
        return view("admin.seats.seat")->with(
            [
                'seats'=>  $seat,
            'locations'=>        $locations,


        ]);




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $seat= seat::all();
        $hall = Hall::all();
        $screentime = showtime::all();
        // if(session()->has('halls')){
        //     $screentime = showtime::where('hall_id',session()->has('halls'))->get();
        //     $h = Hall::where('id',session()->has('halls'))->get();

        //     return view("admin.seats.seatcreate")->with(
        //         ['seats'=>  $seat,

        //             'halls'=>$h,
        //             'screentime' =>       $screentime
        //         ]


        //     );

        // }
        return view("admin.seats.seatcreate")->with(
            ['seats'=>  $seat,

                'halls'=>$hall,
                'screentime' =>       $screentime
            ]


        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    // seat::create(     $request->input());

    $row = $request->row;
    $number = $request->number;
    $s_id = $request->s_id;

    $rownum = $request->numrow;
    $seats = $request->numseat;

    // return    $rownum;
    // rownum A-z if 1 = A, 5 =A,B...,E
    // seat: 1-10 if
    // return $rownum;
    $r ="A";
    for($i=1;$i<=$rownum;$i++){

        for($j = 1;$j<$seats;$j++){
            if($i == 1){
                // return "A";
                $r = "A";
                // return    $r;
            }

            if($i == 2){
                $r = "B";
                // return $i;
            }
            if($i == 3){
                $r = "C";
                // return $i;
            }

            if($i == 4){
                $r = "D";
                // return $i;
            }
            if($i == 5){
                $r = "E";
                // return $i;
            }
            error_log('Some message here.');
            error_log($i);


            // return $r;
            $data =[
                's_id'=>$request->s_id,
                'row'=>  $r ,
                'number'=>$j,
                'price'=>3,

            ];
            DB::table('seat')->insert(    $data);

        }

    }



        // return redirect('/seats');
        return redirect('/admin/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function show(seat $seat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $hall = Hall::all();
        $seat=seat::find($id);
        return view("admin.seats.seatedit")
        ->with(['halls'=>$hall,'seats'=>$seat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
          seat::find($id)->update($request->input());
         return redirect('seats');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        seat::find($id)->delete();
        return redirect('/seats');
    }
}
