

var data='';
for(var i in item){
    data=data+`
        <div class="maincontent-detail">
            <div class="detail-left">
                <img src="${item[i]['pic']}" alt="">
            </div>
            <div class="detail-right">
                <div id="location">
                    <button>${item[i]['locat']}</button>
                </div>
                <div class="text">
                    <span>${item[i]['nameMovie']}</span> ${item[i]['hall']}
                </div>
                <div class="desc">
                    <span>Duration 1h 30mins</span> 3D/English
                </div>
                <span>${item[i]['date']}</span>

                <div class="btn-Time">
                    <button>9:30</button>
                    <button>12:00</button>
                    <button>14:00</button>
                    <button>18:30</button>
                </div>
            </div>
        </div>
    `
}

document.querySelector('.main-detail').innerHTML=data;
