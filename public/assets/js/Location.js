var item=[
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'ToukKork Campus',
        'desc':'3rd Floor, Chip Mong Mega Mall,St271, Phum Prek Ta Nu, Sangkat Chak Angrae Leu, Khan Mean CheyPhnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Toul Campus Tompng 2',
        'desc':'Level 3, City Mall, Building A Monireth Boulevard (St217), Sangkat Vealvong Khan 7 Makara Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Aeon Mall',
        'desc':'Level 3, City Mall, Building A Monireth Boulevard (St217), Sangkat Vealvong Khan 7 Makara Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Steng Mean Chey',
        'desc':'Level 3, The Heritage Walk, Corner of National Road 6 and Oum Chhay Street Svay Dongkoum Commune, Krong Siem Reap Siem Reap, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Siem Reap',
        'desc':'3rd Floor of New Steung Mean Chey market Veng Sreng Blvd , Sangkat Steung Mean Chey, Khan Mean Chey Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Mid Town',
        'desc':'3rd Floor of New Steung Mean Chey market Veng Sreng Blvd , Sangkat Steung Mean Chey, Khan Mean Chey Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Aeon Mall',
        'desc':'4th Floor, Chip Mong SenSok Mall Okhna Mong Reththey street, Khan Sen Sok Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Steng Mean Chey',
        'desc':'5th floor, Chip Mong Noro Mall Preah Norodom Blvd (41) Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'ToukKork Campus',
        'desc':'3rd Floor, Chip Mong Mega Mall,St271, Phum Prek Ta Nu, Sangkat Chak Angrae Leu, Khan Mean CheyPhnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Toul Campus Tompng 2',
        'desc':'Level 3, City Mall, Building A Monireth Boulevard (St217), Sangkat Vealvong Khan 7 Makara Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Aeon Mall',
        'desc':'Level 3, City Mall, Building A Monireth Boulevard (St217), Sangkat Vealvong Khan 7 Makara Phnom Penh, Cambodia'
    },
    {
        'titleMovie1':'Express Cinema',
        'titleMovie2':'Steng Mean Chey',
        'desc':'Level 3, The Heritage Walk, Corner of National Road 6 and Oum Chhay Street Svay Dongkoum Commune, Krong Siem Reap Siem Reap, Cambodia'
    },
    
]

var data='';
for (var i in item){
    data=data+`
        <div class="locatCinema">
            <div class="title">
                <i class="fa-solid fa-location-dot"></i>
                <h5>${item[i]['titleMovie1']} <br> ${item[i]['titleMovie2']}</h5>
            </div> 
            <div class="CapDesc">
                <div class="caption">
                    <button class="btn btn-caption btn-danger">Show Time</button>
                    <button class="btn btn-caption btn-danger">Show Time</button>
                </div>
            </div>
            <div class="desc">
                    <div className="maindesc">
                        <p> ${item[i]['desc']} </p>
                    </div>
            </div>
        </div>  
    `
}

document.querySelector('.content-locat').innerHTML=data;