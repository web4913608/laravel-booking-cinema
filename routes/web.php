<?php

use App\Http\Controllers\HallController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\paymentsController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\SeatController;
use App\Http\Controllers\ShowtimeController;
use App\Http\Controllers\StripeController;
use App\Mail\BookingTicket;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Models\Hall;
use App\Models\Location;
use App\Models\Movie;
use App\Models\Payment;
use App\Models\Reservation;
use App\Models\seat;
use App\Models\showtime;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');
Auth::routes(
    ['verify' => true]
);
//restrictation route for payment and book ticket

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/admin/dashboard', function () {
        if (Auth::id()) { //check if
            $user_role = Auth()->user()->role;
            if ($user_role == 'Admin')
                return view('admin.admin');
        } else {
            return view('page.home');
        }
        return view('page.home');
    });

    Route::get('/order', function () {
        Reservation::all();

        if (!session()->has('seats')) {
            return view('page.no-permit');
        }
        $mid = session()->get('movie_id');
        $st  = session()->get('sid');
        $qty = session()->get('qty');
        $price = session()->get('prices');
        $amt = session()->get('amt');

        $seats = session()->get('seats');

        $movie = Movie::where('id',    $mid)->first();
        session()->put('moviename', $movie->title);
        $show =  showtime::where('id',     $st)->first();


        $sid = array_map('intval',      $seats);
        $seatrow = seat::whereIn('id',    $sid)->get();

        //   return $seatrow;


        session()->put('seatrow',   $seatrow);
        return    view('page.payment')->with(
            [
                'movie' => $movie,
                'show' => $show,
                'qty' => $qty,
                'price' => $price,
                'amt' =>    $amt,
                'seats' => $seats,
                'seatrow' =>      $seatrow
            ]
        );
    });

    Route::get('/payment/success', function (Request $request) {


        if (!session()->get('booking_id')) {
            return view('page.no-permit');
        }
        $bid = session()->get('booking_id');

        $p = Payment::where('id',    $bid)->first();

        $booking = Reservation::where(
            'id',
            $bid
        )->first();
        $users = User::where('id', $booking->user_id)->first();
        $customername = $users->name;
        $customeremail = $users->email;
        // return  $users;

        // return  $booking;
        $showtime =  showtime::where('id',   $booking->show_id)->first();

        $seats = session()->get('seats');
        $sid = array_map('intval',      $seats);
        $seatrow = seat::whereIn('id',    $sid)->get();

        // return $booking->booking_ticket;

        // Mail::send('Booking Completed',)

        // return       $seatrow;

        //mail controller here

        // $ticket = '21321123';

        Mail::to($users->email)->send(new BookingTicket($booking->booking_ticket));


        return   view("page.success")->with([
            'payment' => $p,
            'reserve' =>    $booking,
            'showtime' =>        $showtime,
            'seatrow' =>    $seatrow,
            'customername' => $customername,
            'customeremail' =>        $customeremail
        ]);
    });
    Route::post('/order/post', function (Request $request) {
        Reservation::all();
        // $request->input();
        $seats = $request->all();


        $sid = request('show_id');

        $mid = request('movie_id');

        $qty = request('qty');

        $price = request('price');

        $amt = request('totalprice');

        $ss = request('seat_chart');





        session()->put('sid', $sid);
        session()->put('movie_id',    $mid);
        session()->put('qty', $qty);
        session()->put('prices', $price);
        session()->put('amt',    $amt);
        session()->put('seats',    $ss);
        // $selectedseat = session()->get('seats');
        $sid = array_map('intval',     $ss);
        // $seatrow = seat::whereIn('id',    $sid  )->get();
        // return         $seatrow ;
        return   redirect('/order');
    });




    // Route::post('/payment',function(Request $request){
    //     // Reservation::all();
    //     // $request->input();
    //      $reserve = new Reservation;
    //      $reserve->price =  request('price');
    //      $reserve->qty =  request('qty');
    //      $selectedseat = session()->get('seats');
    //      $sid = array_map('intval',  $selectedseat );
    //      //map string arr to int

    //     //   return $seatrow;


    //      $reserve->seats =       $sid;
    //      $reserve->show_id =   session()->get('sid');
    //      $reserve->save();
    //     seat::whereIn('id',  $sid )->update([
    //         'active' => false
    //      ]);
    //     //  $reserve->seats =  request('price');
    //     // return     Reservation::all();

    //     return   redirect('/payment/success');
    // });

    Route::resource('/payment',  paymentsController::class);

    Route::get('/search/ticket', [ReservationController::class, 'search'])->name('ticket');
});


//add auth for verificatio email
Route::get('/verify', function () {
    return redirect('/');
})->middleware('verified');

Route::get('/', function () {

    session()->pull('soon');
    session()->push('now', true);



    $role = User::where('id', session()->get('uid'))->first();
    //  return $role;

    $movie = Movie::where('state', '=', 'now')
        ->where('status', 'LIKE', 'on-going')
        ->orderBy('id', 'Desc')->get();


    if ($role) {

        return view('page.home', ['movies' => $movie, 'role' => $role->role]);
    }
    return view('page.home', ['movies' => $movie]);
});

Route::get('/movies/search', [MovieController::class, 'search']);

Route::get('/search', function () {

    //  $movie = Movie::all();
    session()->pull('soon');
    session()->push('now', true);



    $role = User::where('id', session()->get('uid'))->first();
    //  return $role;
    if (request('title')) {
        $title = request('title');
        $movie = Movie::where('title', 'LIKE', "%" .      $title . "%")->orderBy('id', 'Desc')->get();
        // return $movie;
    } else {
        return redirect('/');
    }



    if ($role) {

        return view('page.home', ['movies' => $movie, 'role' => $role->role]);
    }
    return view('page.home', ['movies' => $movie]);
});

Route::get('/location', function () {


    return view("page.Location");
});

Route::get('/screentime', function () {


    $screentime = showtime::all();
    $md = showtime::select('movie_date')->get();

    return view("page.Schedule")->with([
        'screentime' =>  $screentime,
        'mdate' => $md
    ]);
});

Route::get('/comingsoon', function () {

    session()->pull('now');
    session()->push('soon', true);
    $movie = Movie::all();
    $movie = Movie::where('state', '=', 'comingsoon')->orderBy('id', 'Desc')->get();
    //  $movie = Movie::orderBy('id','Desc')->get();

    return view('page.home', ['movies' => $movie]);
});

Route::get('/admin/dashboard', function () {
    if (Auth::id()) { //check if
        $user_role = Auth()->user()->role;
        if ($user_role == 'Client')
            return view('page.no-permit');
        else {
            return view('admin.dashboard');
        }
    } else {
        return view('page.home');
    }
})->middleware('auth');

Route::get('/movie/detail', function (Request $request) {

    if (request('id')) {
        $id =  request('id');
        $movie = DB::table('movies')->where('id', $id)->first();
        $showtime = showtime::where('trash', '=', 0)->orderBy('screentime', 'DESC')->get();
        $md = showtime::where('trash', '=', 0)
            ->orderBy('movie_date', 'ASC')
            ->select('movie_date')->groupBy('movie_date')->get();

        //   $skip = request('page');
        //   $mdt = showtime::orderBy('id')->skip(0)->take(1)->get();
        $mdt = showtime::orderBy('movie_date')->first();


        $querydate = request('date');
        if ($md->count() > 0) {
            if (!$querydate) {
                $date = showtime::orderBy('movie_date')->first();
                $querydate = $date->movie_date;
            }
        }


        $screentime = showtime::where('movie_id', $id)



            ->where('movie_date', 'LIKE', $querydate)->groupBy('hall_id')
            ->orderBy('screentime', 'DESC')->get();



        return view("page.Schedule")->with([
            'screentime' =>  $screentime,
            'movie' =>    $movie,
            'showtime' =>     $showtime,
            'mdate' => $md,
            'latestdate' =>  $mdt,
            'sldate' =>     $querydate
        ]);
    }

    // $movie = DB::table('movies')->where('id', $id)->first();

    $movie = Movie::all();
    $showtime = showtime::where('trash', '=', 0)->orderBy('screentime', 'DESC')->get();
    $md = showtime::where('trash', '=', 0)
        ->orderBy('movie_date', 'ASC')
        ->select('movie_date')->groupBy('movie_date')->get();

    //   $skip = request('page');
    //   $mdt = showtime::orderBy('id')->skip(0)->take(1)->get();
    $mdt = showtime::orderBy('id')->first();



    //   $querydate = request('date');
    $querydate = request('date');
    if ($md->count() > 0) {
        if (!$querydate) {
            $date = showtime::orderBy('movie_date')->first();
            $querydate = $date->movie_date;
        }
    }
    $screentime = showtime::groupBy('hall_id')->where('movie_date', 'LIKE', $querydate)->groupBy('hall_id')
        ->orderBy('screentime', 'DESC')->get();
    if ($md->count() > 0) {
        if (!$querydate) {
            $date = showtime::orderBy('movie_date')->first();
            $querydate = $date->movie_date;
        }
    }






    if (request('location')) {

        $hid = Hall::where('location_id', request('location'))->first();

        $ss = showtime::where('hall_id',     '=', $hid->id)

            ->where('movie_date', 'LIKE', $querydate)

            ->orderBy('screentime', 'DESC')->get();

        if ($md->count() > 0) {
            if (!$querydate) {
                $date = showtime::orderBy('movie_date')->first();
                $querydate = $date->movie_date;
            }
        }
        return view("page.showtime")->with([
            'screentime' =>  $ss,
            'movie' =>    $movie,
            'showtime' =>     $showtime,
            'mdate' => $md,
            'latestdate' =>  $mdt,
            'sldate' =>     $querydate
        ]);
    }

    //   $screentime = showtime::all();
    //   return $screentime;

    return view("page.showtime")->with([
        'screentime' =>  $screentime,
        'movie' =>    $movie,
        'showtime' =>     $showtime,
        'mdate' => $md,
        'latestdate' =>  $mdt,
        'sldate' =>     $querydate
    ]);
});

Route::get('/movie/showtime', function (Request $request) {

    $hallid = request('hallid');
    $time = request('time');
    $sid = request('sid');
    $movieid = request('id');
    // return [$movieid,$hallid,$time];
    // $seattime = seat::where('hall_id',$hallid)
    // ->where('hall.',$movieid)
    $sql = "SELECT t.id, t.* FROM seat t
    inner join _showtime s on t.s_id = s.id

    where s.id =" . $sid . " and s.movie_id=" . $movieid . "
    and s.screentime LIKE '" . $time . "' order by t.row;";

    $seattime = DB::select(
        DB::raw(
            $sql
        )
    );
    //   return    $seattime;
    // ->get();
    // $seattime = DB::select(     DB::raw("SELECT t.id,s.hall_id, s.movie_id,s.screentime FROM seat_tables t
    // inner join _showtime s on t.hall_id = s.hall_id

    // where s.hall_id = 1 and s.movie_id = 1 and s.screentime LIKE '13:20';"));
    // return    $seattime ;
    $hallname = hall::where('id', $hallid)->first();
    $movies = Movie::where('id', $movieid)->first();

    return view("page.movie_detail")->with(
        [
            'seats' => $seattime,
            'srctime' => $time,
            'hallname' => $hallname,
            'movie' => $movies
        ]

    );
});


Route::get('/location/showtime', function (Request $request) {
    $request->session()->put('select', true);
    $l = Location::all();
    return view('page.Location')->with('locations', $l);
});


Route::resource('/movies',  MovieController::class);


Route::resource('/locations',  LocationController::class);
Route::resource('/halls',  HallController::class);
Route::resource('/booking',  ReservationController::class);
Route::resource('/seats',  SeatController::class);
Route::get('/showtime/search', [ShowtimeController::class, 'search']);
Route::resource('/showtime',  ShowtimeController::class);



Route::get('/movie/payment', function () {
    return view('page.payment');
});
Route::resource('/payment',  paymentsController::class);


Route::post('/seats/location', function (Request $request) {
    session()->pull('hid');
    $locationid = $request->input('location');
    // return $locationid;
    $halls = Hall::where('location_id', $locationid)->first();
    if ($halls) {
        $hid = $halls->id;


        // return $showtime;
        session()->put('hid',   $hid);
        return redirect()->back();
    }
    return redirect()->back();

    //  return      $showtime ;
    // return   $request->input('location');

});

Route::post('/seats/showtime', function (Request $request) {
    $sid = $request->input('show_id');
    $screentime = showtime::where('id', $sid)->first();
    //    return $screentime;
    session()->put('srt', $screentime->screentime);
    session()->pull('hid');
    return redirect()->back();

    //  return      $showtime ;
    // return   $request->input('location');

});

Route::get('/movie/showtime/location', function (Request $request) {

    $hallid = request('hallid');
    $time = request('time');
    $sid = request('sid');
    $movieid = request('id');
    // return [$movieid,$hallid,$time];
    // $seattime = seat::where('hall_id',$hallid)
    // ->where('hall.',$movieid)
    $sql = "SELECT t.id, t.* FROM seat t
    inner join _showtime s on t.s_id = s.id

    where s.id =" . $sid . " and s.movie_id=" . $movieid . "
    and s.screentime LIKE '" . $time . "' order by t.row;";

    $seattime = DB::select(
        DB::raw(
            $sql
        )
    );
    //   return    $seattime;
    // ->get();
    // $seattime = DB::select(     DB::raw("SELECT t.id,s.hall_id, s.movie_id,s.screentime FROM seat_tables t
    // inner join _showtime s on t.hall_id = s.hall_id

    // where s.hall_id = 1 and s.movie_id = 1 and s.screentime LIKE '13:20';"));
    // return    $seattime ;
    $hallname = hall::where('id', $hallid)->first();
    $movies = Movie::where('id', $movieid)->first();

    return view("page.movie_detail")->with(
        [
            'seats' => $seattime,
            'srctime' => $time,
            'hallname' => $hallname,
            'movie' => $movies
        ]

    );
});

Route::get('/checkout', [StripeController::class, 'checkout'])->name('checkout');

Route::post('/session', [StripeController::class, 'session'])->name('session');

Route::get('/success', [StripeController::class, 'success'])->name('success');
Route::post('/session', [StripeController::class, 'session'])->name('session');

Route::get('/success', [StripeController::class, 'success'])->name('success');

require __DIR__ . '/auth.php';
