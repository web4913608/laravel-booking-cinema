<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_showtime', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('hall_id')->index();

            $table->boolean('status');
            $table->foreign('hall_id')->
            references('id')->on('hall_tables');


            $table->unsignedBigInteger('movie_id')->index();
            $table->foreign('movie_id')->
            references('id')->on('movies');

            $table->string('screentime');
            $table->string('movie_date');
            $table->boolean('trash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_showtime');
    }
};
