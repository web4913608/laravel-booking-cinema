<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            // $table->timestamps();
            $table->string('method');
            $table->string('discount');
            $table->float('amount');
            $table->unsignedBigInteger('booking_id')->index();
            $table->foreign('booking_id')->references('id')
            ->on('reservations');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
