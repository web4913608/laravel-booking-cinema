<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //order detail
        Schema::create('reservations', function (Blueprint $table) {

            $table->id();

            $table->engine = 'InnoDB';

            $table->unsignedBigInteger('show_id')->index()->nullable();;

            $table->foreign('show_id')->references('id')->

            on('_showtime')->onDelete('set null');;

            $table->timestamps();

            $table->float('price');

            $table->integer('qty');

            $table->json('seats');

            $table->string('booking_ticket');

            $table->boolean('paid');
            //paid = 1 paid, 0 reserved or book

            $table->unsignedBigInteger('user_id')->index();

            $table->foreign('user_id')->references('id')->

            on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
};
