<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
                //
        DB::table('movies')->insert([
                    'title' =>'Fun Mall',
                    'director' => 'Phnom Penh',
                    'description' => '#7, St.7 S/K Kampong Cham, Kampong Chaam City',
                    'duration' => '#7, St.7 S/K Kampong Cham, Kampong Chaam City',
                    'movie_date' => '#7, St.7 S/K Kampong Cham, Kampong Chaam City',
                    'state'=>'now',
                    'genre'=>'comedy',
                    'languages'=>'English'

                ]);
    }
}
