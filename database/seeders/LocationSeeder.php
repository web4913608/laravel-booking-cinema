<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('locations')->insert([
            'district' =>'Fun Mall',
            'city' => 'Phnom Penh',
            'address' => '#7, St.7 S/K Kampong Cham, Kampong Chaam City'

        ]);
    }
}
